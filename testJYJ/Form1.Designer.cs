﻿
namespace testJYJ
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btn수집중지 = new System.Windows.Forms.Button();
            this.btn정보설정1 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.txt현재카운트2 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txt현재카운트3 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txt현재카운트4 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txt현재카운트5 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txt현재카운트6 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txt현재카운트1 = new System.Windows.Forms.TextBox();
            this.txt최종시간6 = new System.Windows.Forms.TextBox();
            this.txt최종시간5 = new System.Windows.Forms.TextBox();
            this.txt최종시간4 = new System.Windows.Forms.TextBox();
            this.txt최종시간3 = new System.Windows.Forms.TextBox();
            this.txt최종시간2 = new System.Windows.Forms.TextBox();
            this.txt최종시간1 = new System.Windows.Forms.TextBox();
            this.txt직전카운트6 = new System.Windows.Forms.TextBox();
            this.txt직전카운트5 = new System.Windows.Forms.TextBox();
            this.txt직전카운트4 = new System.Windows.Forms.TextBox();
            this.txt직전카운트3 = new System.Windows.Forms.TextBox();
            this.txt직전카운트2 = new System.Windows.Forms.TextBox();
            this.txt직전카운트1 = new System.Windows.Forms.TextBox();
            this.txt직전시간6 = new System.Windows.Forms.TextBox();
            this.txt직전시간5 = new System.Windows.Forms.TextBox();
            this.txt직전시간4 = new System.Windows.Forms.TextBox();
            this.txt직전시간3 = new System.Windows.Forms.TextBox();
            this.txt직전시간2 = new System.Windows.Forms.TextBox();
            this.txt직전시간1 = new System.Windows.Forms.TextBox();
            this.txt설비코드6 = new System.Windows.Forms.TextBox();
            this.txt설비코드5 = new System.Windows.Forms.TextBox();
            this.txt설비코드4 = new System.Windows.Forms.TextBox();
            this.txt설비코드3 = new System.Windows.Forms.TextBox();
            this.txt설비코드2 = new System.Windows.Forms.TextBox();
            this.txt설비코드1 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.btn수집시작 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.mySqlConnection1 = new MySql.Data.MySqlClient.MySqlConnection();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label22 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button9);
            this.groupBox1.Controls.Add(this.button8);
            this.groupBox1.Controls.Add(this.button7);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.btn수집중지);
            this.groupBox1.Controls.Add(this.btn정보설정1);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.txt현재카운트2);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.txt현재카운트3);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.txt현재카운트4);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.txt현재카운트5);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.txt현재카운트6);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.txt현재카운트1);
            this.groupBox1.Controls.Add(this.txt최종시간6);
            this.groupBox1.Controls.Add(this.txt최종시간5);
            this.groupBox1.Controls.Add(this.txt최종시간4);
            this.groupBox1.Controls.Add(this.txt최종시간3);
            this.groupBox1.Controls.Add(this.txt최종시간2);
            this.groupBox1.Controls.Add(this.txt최종시간1);
            this.groupBox1.Controls.Add(this.txt직전카운트6);
            this.groupBox1.Controls.Add(this.txt직전카운트5);
            this.groupBox1.Controls.Add(this.txt직전카운트4);
            this.groupBox1.Controls.Add(this.txt직전카운트3);
            this.groupBox1.Controls.Add(this.txt직전카운트2);
            this.groupBox1.Controls.Add(this.txt직전카운트1);
            this.groupBox1.Controls.Add(this.txt직전시간6);
            this.groupBox1.Controls.Add(this.txt직전시간5);
            this.groupBox1.Controls.Add(this.txt직전시간4);
            this.groupBox1.Controls.Add(this.txt직전시간3);
            this.groupBox1.Controls.Add(this.txt직전시간2);
            this.groupBox1.Controls.Add(this.txt직전시간1);
            this.groupBox1.Controls.Add(this.txt설비코드6);
            this.groupBox1.Controls.Add(this.txt설비코드5);
            this.groupBox1.Controls.Add(this.txt설비코드4);
            this.groupBox1.Controls.Add(this.txt설비코드3);
            this.groupBox1.Controls.Add(this.txt설비코드2);
            this.groupBox1.Controls.Add(this.txt설비코드1);
            this.groupBox1.Controls.Add(this.textBox8);
            this.groupBox1.Controls.Add(this.textBox7);
            this.groupBox1.Controls.Add(this.textBox6);
            this.groupBox1.Controls.Add(this.textBox5);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.btn수집시작);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(10, 47);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(1053, 444);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.RoyalBlue;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button9.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.Location = new System.Drawing.Point(12, 326);
            this.button9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(99, 28);
            this.button9.TabIndex = 73;
            this.button9.Text = "정보설정";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.RoyalBlue;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button8.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button8.ForeColor = System.Drawing.Color.White;
            this.button8.Location = new System.Drawing.Point(12, 283);
            this.button8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(99, 28);
            this.button8.TabIndex = 72;
            this.button8.Text = "정보설정";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.RoyalBlue;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button7.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Location = new System.Drawing.Point(12, 242);
            this.button7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(99, 28);
            this.button7.TabIndex = 71;
            this.button7.Text = "정보설정";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.RoyalBlue;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button6.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(12, 198);
            this.button6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(99, 28);
            this.button6.TabIndex = 70;
            this.button6.Text = "정보설정";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.RoyalBlue;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(12, 154);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 28);
            this.button1.TabIndex = 69;
            this.button1.Text = "정보설정";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn수집중지
            // 
            this.btn수집중지.BackColor = System.Drawing.Color.Transparent;
            this.btn수집중지.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn수집중지.Location = new System.Drawing.Point(393, 26);
            this.btn수집중지.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn수집중지.Name = "btn수집중지";
            this.btn수집중지.Size = new System.Drawing.Size(94, 27);
            this.btn수집중지.TabIndex = 68;
            this.btn수집중지.Text = "수집중지";
            this.btn수집중지.UseVisualStyleBackColor = false;
            this.btn수집중지.Click += new System.EventHandler(this.btn수집중지_Click);
            // 
            // btn정보설정1
            // 
            this.btn정보설정1.BackColor = System.Drawing.Color.RoyalBlue;
            this.btn정보설정1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn정보설정1.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn정보설정1.ForeColor = System.Drawing.Color.White;
            this.btn정보설정1.Location = new System.Drawing.Point(12, 110);
            this.btn정보설정1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn정보설정1.Name = "btn정보설정1";
            this.btn정보설정1.Size = new System.Drawing.Size(99, 28);
            this.btn정보설정1.TabIndex = 62;
            this.btn정보설정1.Text = "정보설정";
            this.btn정보설정1.UseVisualStyleBackColor = false;
            this.btn정보설정1.Click += new System.EventHandler(this.btn정보설정_Click);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button5.Location = new System.Drawing.Point(900, 381);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(109, 45);
            this.button5.TabIndex = 61;
            this.button5.Text = "종료하기";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button4.Location = new System.Drawing.Point(655, 381);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(239, 45);
            this.button4.TabIndex = 60;
            this.button4.Text = "생산카운터 수집하기";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button2.Location = new System.Drawing.Point(383, 381);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(239, 45);
            this.button2.TabIndex = 59;
            this.button2.Text = "PLC-NO, 설비코드 반영하기";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button3.Location = new System.Drawing.Point(144, 381);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(230, 45);
            this.button3.TabIndex = 58;
            this.button3.Text = "PLC-NO, 설비코드 초기화";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // txt현재카운트2
            // 
            this.txt현재카운트2.BackColor = System.Drawing.Color.White;
            this.txt현재카운트2.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt현재카운트2.Location = new System.Drawing.Point(858, 157);
            this.txt현재카운트2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt현재카운트2.Name = "txt현재카운트2";
            this.txt현재카운트2.Size = new System.Drawing.Size(107, 26);
            this.txt현재카운트2.TabIndex = 57;
            this.txt현재카운트2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(1000, 160);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(20, 16);
            this.label21.TabIndex = 56;
            this.label21.Text = "X";
            // 
            // txt현재카운트3
            // 
            this.txt현재카운트3.BackColor = System.Drawing.Color.White;
            this.txt현재카운트3.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt현재카운트3.Location = new System.Drawing.Point(858, 202);
            this.txt현재카운트3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt현재카운트3.Name = "txt현재카운트3";
            this.txt현재카운트3.Size = new System.Drawing.Size(107, 26);
            this.txt현재카운트3.TabIndex = 55;
            this.txt현재카운트3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(1000, 205);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(20, 16);
            this.label20.TabIndex = 54;
            this.label20.Text = "X";
            // 
            // txt현재카운트4
            // 
            this.txt현재카운트4.BackColor = System.Drawing.Color.White;
            this.txt현재카운트4.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt현재카운트4.Location = new System.Drawing.Point(858, 246);
            this.txt현재카운트4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt현재카운트4.Name = "txt현재카운트4";
            this.txt현재카운트4.Size = new System.Drawing.Size(107, 26);
            this.txt현재카운트4.TabIndex = 53;
            this.txt현재카운트4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(1000, 249);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(20, 16);
            this.label19.TabIndex = 52;
            this.label19.Text = "X";
            // 
            // txt현재카운트5
            // 
            this.txt현재카운트5.BackColor = System.Drawing.Color.White;
            this.txt현재카운트5.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt현재카운트5.Location = new System.Drawing.Point(858, 286);
            this.txt현재카운트5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt현재카운트5.Name = "txt현재카운트5";
            this.txt현재카운트5.Size = new System.Drawing.Size(107, 26);
            this.txt현재카운트5.TabIndex = 51;
            this.txt현재카운트5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(1000, 290);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(20, 16);
            this.label18.TabIndex = 50;
            this.label18.Text = "X";
            // 
            // txt현재카운트6
            // 
            this.txt현재카운트6.BackColor = System.Drawing.Color.White;
            this.txt현재카운트6.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt현재카운트6.Location = new System.Drawing.Point(858, 328);
            this.txt현재카운트6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt현재카운트6.Name = "txt현재카운트6";
            this.txt현재카운트6.Size = new System.Drawing.Size(107, 26);
            this.txt현재카운트6.TabIndex = 49;
            this.txt현재카운트6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(1000, 331);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(20, 16);
            this.label17.TabIndex = 48;
            this.label17.Text = "X";
            // 
            // txt현재카운트1
            // 
            this.txt현재카운트1.BackColor = System.Drawing.Color.White;
            this.txt현재카운트1.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt현재카운트1.Location = new System.Drawing.Point(858, 113);
            this.txt현재카운트1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt현재카운트1.Name = "txt현재카운트1";
            this.txt현재카운트1.Size = new System.Drawing.Size(107, 26);
            this.txt현재카운트1.TabIndex = 47;
            this.txt현재카운트1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt최종시간6
            // 
            this.txt최종시간6.BackColor = System.Drawing.Color.White;
            this.txt최종시간6.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt최종시간6.Location = new System.Drawing.Point(666, 328);
            this.txt최종시간6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt최종시간6.Name = "txt최종시간6";
            this.txt최종시간6.Size = new System.Drawing.Size(182, 26);
            this.txt최종시간6.TabIndex = 46;
            // 
            // txt최종시간5
            // 
            this.txt최종시간5.BackColor = System.Drawing.Color.White;
            this.txt최종시간5.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt최종시간5.Location = new System.Drawing.Point(666, 286);
            this.txt최종시간5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt최종시간5.Name = "txt최종시간5";
            this.txt최종시간5.Size = new System.Drawing.Size(182, 26);
            this.txt최종시간5.TabIndex = 45;
            // 
            // txt최종시간4
            // 
            this.txt최종시간4.BackColor = System.Drawing.Color.White;
            this.txt최종시간4.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt최종시간4.Location = new System.Drawing.Point(666, 246);
            this.txt최종시간4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt최종시간4.Name = "txt최종시간4";
            this.txt최종시간4.Size = new System.Drawing.Size(182, 26);
            this.txt최종시간4.TabIndex = 44;
            // 
            // txt최종시간3
            // 
            this.txt최종시간3.BackColor = System.Drawing.Color.White;
            this.txt최종시간3.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt최종시간3.Location = new System.Drawing.Point(666, 202);
            this.txt최종시간3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt최종시간3.Name = "txt최종시간3";
            this.txt최종시간3.Size = new System.Drawing.Size(182, 26);
            this.txt최종시간3.TabIndex = 43;
            // 
            // txt최종시간2
            // 
            this.txt최종시간2.BackColor = System.Drawing.Color.White;
            this.txt최종시간2.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt최종시간2.Location = new System.Drawing.Point(666, 157);
            this.txt최종시간2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt최종시간2.Name = "txt최종시간2";
            this.txt최종시간2.Size = new System.Drawing.Size(182, 26);
            this.txt최종시간2.TabIndex = 42;
            // 
            // txt최종시간1
            // 
            this.txt최종시간1.BackColor = System.Drawing.Color.White;
            this.txt최종시간1.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt최종시간1.Location = new System.Drawing.Point(666, 113);
            this.txt최종시간1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt최종시간1.Name = "txt최종시간1";
            this.txt최종시간1.Size = new System.Drawing.Size(182, 26);
            this.txt최종시간1.TabIndex = 41;
            // 
            // txt직전카운트6
            // 
            this.txt직전카운트6.BackColor = System.Drawing.Color.White;
            this.txt직전카운트6.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt직전카운트6.Location = new System.Drawing.Point(547, 328);
            this.txt직전카운트6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt직전카운트6.Name = "txt직전카운트6";
            this.txt직전카운트6.Size = new System.Drawing.Size(107, 26);
            this.txt직전카운트6.TabIndex = 40;
            this.txt직전카운트6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt직전카운트5
            // 
            this.txt직전카운트5.BackColor = System.Drawing.Color.White;
            this.txt직전카운트5.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt직전카운트5.Location = new System.Drawing.Point(547, 286);
            this.txt직전카운트5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt직전카운트5.Name = "txt직전카운트5";
            this.txt직전카운트5.Size = new System.Drawing.Size(107, 26);
            this.txt직전카운트5.TabIndex = 39;
            this.txt직전카운트5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt직전카운트4
            // 
            this.txt직전카운트4.BackColor = System.Drawing.Color.White;
            this.txt직전카운트4.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt직전카운트4.Location = new System.Drawing.Point(547, 246);
            this.txt직전카운트4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt직전카운트4.Name = "txt직전카운트4";
            this.txt직전카운트4.Size = new System.Drawing.Size(107, 26);
            this.txt직전카운트4.TabIndex = 38;
            this.txt직전카운트4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt직전카운트3
            // 
            this.txt직전카운트3.BackColor = System.Drawing.Color.White;
            this.txt직전카운트3.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt직전카운트3.Location = new System.Drawing.Point(547, 202);
            this.txt직전카운트3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt직전카운트3.Name = "txt직전카운트3";
            this.txt직전카운트3.Size = new System.Drawing.Size(107, 26);
            this.txt직전카운트3.TabIndex = 37;
            this.txt직전카운트3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt직전카운트2
            // 
            this.txt직전카운트2.BackColor = System.Drawing.Color.White;
            this.txt직전카운트2.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt직전카운트2.Location = new System.Drawing.Point(547, 157);
            this.txt직전카운트2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt직전카운트2.Name = "txt직전카운트2";
            this.txt직전카운트2.Size = new System.Drawing.Size(107, 26);
            this.txt직전카운트2.TabIndex = 36;
            this.txt직전카운트2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt직전카운트1
            // 
            this.txt직전카운트1.BackColor = System.Drawing.Color.White;
            this.txt직전카운트1.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt직전카운트1.Location = new System.Drawing.Point(547, 113);
            this.txt직전카운트1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt직전카운트1.Name = "txt직전카운트1";
            this.txt직전카운트1.Size = new System.Drawing.Size(107, 26);
            this.txt직전카운트1.TabIndex = 35;
            this.txt직전카운트1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt직전시간6
            // 
            this.txt직전시간6.BackColor = System.Drawing.Color.White;
            this.txt직전시간6.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt직전시간6.Location = new System.Drawing.Point(355, 328);
            this.txt직전시간6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt직전시간6.Name = "txt직전시간6";
            this.txt직전시간6.Size = new System.Drawing.Size(182, 26);
            this.txt직전시간6.TabIndex = 34;
            // 
            // txt직전시간5
            // 
            this.txt직전시간5.BackColor = System.Drawing.Color.White;
            this.txt직전시간5.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt직전시간5.Location = new System.Drawing.Point(355, 286);
            this.txt직전시간5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt직전시간5.Name = "txt직전시간5";
            this.txt직전시간5.Size = new System.Drawing.Size(182, 26);
            this.txt직전시간5.TabIndex = 33;
            // 
            // txt직전시간4
            // 
            this.txt직전시간4.BackColor = System.Drawing.Color.White;
            this.txt직전시간4.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt직전시간4.Location = new System.Drawing.Point(355, 246);
            this.txt직전시간4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt직전시간4.Name = "txt직전시간4";
            this.txt직전시간4.Size = new System.Drawing.Size(182, 26);
            this.txt직전시간4.TabIndex = 32;
            // 
            // txt직전시간3
            // 
            this.txt직전시간3.BackColor = System.Drawing.Color.White;
            this.txt직전시간3.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt직전시간3.Location = new System.Drawing.Point(355, 202);
            this.txt직전시간3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt직전시간3.Name = "txt직전시간3";
            this.txt직전시간3.Size = new System.Drawing.Size(182, 26);
            this.txt직전시간3.TabIndex = 31;
            // 
            // txt직전시간2
            // 
            this.txt직전시간2.BackColor = System.Drawing.Color.White;
            this.txt직전시간2.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt직전시간2.Location = new System.Drawing.Point(355, 157);
            this.txt직전시간2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt직전시간2.Name = "txt직전시간2";
            this.txt직전시간2.Size = new System.Drawing.Size(182, 26);
            this.txt직전시간2.TabIndex = 30;
            // 
            // txt직전시간1
            // 
            this.txt직전시간1.BackColor = System.Drawing.Color.White;
            this.txt직전시간1.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt직전시간1.Location = new System.Drawing.Point(355, 113);
            this.txt직전시간1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt직전시간1.Name = "txt직전시간1";
            this.txt직전시간1.Size = new System.Drawing.Size(182, 26);
            this.txt직전시간1.TabIndex = 29;
            // 
            // txt설비코드6
            // 
            this.txt설비코드6.BackColor = System.Drawing.Color.PowderBlue;
            this.txt설비코드6.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt설비코드6.Location = new System.Drawing.Point(266, 328);
            this.txt설비코드6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt설비코드6.Name = "txt설비코드6";
            this.txt설비코드6.Size = new System.Drawing.Size(81, 26);
            this.txt설비코드6.TabIndex = 28;
            // 
            // txt설비코드5
            // 
            this.txt설비코드5.BackColor = System.Drawing.Color.PowderBlue;
            this.txt설비코드5.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt설비코드5.Location = new System.Drawing.Point(266, 286);
            this.txt설비코드5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt설비코드5.Name = "txt설비코드5";
            this.txt설비코드5.Size = new System.Drawing.Size(81, 26);
            this.txt설비코드5.TabIndex = 27;
            // 
            // txt설비코드4
            // 
            this.txt설비코드4.BackColor = System.Drawing.Color.PowderBlue;
            this.txt설비코드4.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt설비코드4.Location = new System.Drawing.Point(266, 246);
            this.txt설비코드4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt설비코드4.Name = "txt설비코드4";
            this.txt설비코드4.Size = new System.Drawing.Size(81, 26);
            this.txt설비코드4.TabIndex = 26;
            // 
            // txt설비코드3
            // 
            this.txt설비코드3.BackColor = System.Drawing.Color.PowderBlue;
            this.txt설비코드3.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt설비코드3.Location = new System.Drawing.Point(266, 202);
            this.txt설비코드3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt설비코드3.Name = "txt설비코드3";
            this.txt설비코드3.Size = new System.Drawing.Size(81, 26);
            this.txt설비코드3.TabIndex = 25;
            // 
            // txt설비코드2
            // 
            this.txt설비코드2.BackColor = System.Drawing.Color.PowderBlue;
            this.txt설비코드2.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt설비코드2.Location = new System.Drawing.Point(266, 157);
            this.txt설비코드2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt설비코드2.Name = "txt설비코드2";
            this.txt설비코드2.Size = new System.Drawing.Size(81, 26);
            this.txt설비코드2.TabIndex = 24;
            // 
            // txt설비코드1
            // 
            this.txt설비코드1.BackColor = System.Drawing.Color.PowderBlue;
            this.txt설비코드1.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt설비코드1.Location = new System.Drawing.Point(266, 113);
            this.txt설비코드1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt설비코드1.Name = "txt설비코드1";
            this.txt설비코드1.Size = new System.Drawing.Size(81, 26);
            this.txt설비코드1.TabIndex = 23;
            // 
            // textBox8
            // 
            this.textBox8.BackColor = System.Drawing.Color.PowderBlue;
            this.textBox8.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBox8.Location = new System.Drawing.Point(208, 157);
            this.textBox8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(42, 26);
            this.textBox8.TabIndex = 22;
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.Color.PowderBlue;
            this.textBox7.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBox7.Location = new System.Drawing.Point(208, 202);
            this.textBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(42, 26);
            this.textBox7.TabIndex = 21;
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.Color.PowderBlue;
            this.textBox6.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBox6.Location = new System.Drawing.Point(208, 246);
            this.textBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(42, 26);
            this.textBox6.TabIndex = 20;
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.PowderBlue;
            this.textBox5.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBox5.Location = new System.Drawing.Point(208, 286);
            this.textBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(42, 26);
            this.textBox5.TabIndex = 19;
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.Color.PowderBlue;
            this.textBox4.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBox4.Location = new System.Drawing.Point(208, 328);
            this.textBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(42, 26);
            this.textBox4.TabIndex = 18;
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.PowderBlue;
            this.textBox3.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBox3.Location = new System.Drawing.Point(208, 113);
            this.textBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(42, 26);
            this.textBox3.TabIndex = 17;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(1000, 116);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(20, 16);
            this.label15.TabIndex = 16;
            this.label15.Text = "X";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.Location = new System.Drawing.Point(121, 330);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(66, 16);
            this.label14.TabIndex = 15;
            this.label14.Text = "MCH-06";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.Location = new System.Drawing.Point(121, 289);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(66, 16);
            this.label13.TabIndex = 14;
            this.label13.Text = "MCH-05";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.Location = new System.Drawing.Point(121, 248);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 16);
            this.label12.TabIndex = 13;
            this.label12.Text = "MCH-04";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(121, 204);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 16);
            this.label11.TabIndex = 12;
            this.label11.Text = "MCH-03";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(121, 159);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 16);
            this.label10.TabIndex = 11;
            this.label10.Text = "MCH-02";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(121, 115);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 16);
            this.label9.TabIndex = 10;
            this.label9.Text = "MCH-01";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(403, 84);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 16);
            this.label8.TabIndex = 9;
            this.label8.Text = "직전시간";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(718, 84);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 16);
            this.label7.TabIndex = 8;
            this.label7.Text = "최종시각";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(553, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 16);
            this.label6.TabIndex = 7;
            this.label6.Text = "직전카운터";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(988, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 16);
            this.label5.TabIndex = 6;
            this.label5.Text = "가동";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(864, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "현재카운터";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(269, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "설비코드";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(192, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "PLC-NO";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.LavenderBlush;
            this.textBox2.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBox2.Location = new System.Drawing.Point(496, 30);
            this.textBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(552, 26);
            this.textBox2.TabIndex = 2;
            this.textBox2.Text = "연결된 설비가 없습니다.";
            // 
            // btn수집시작
            // 
            this.btn수집시작.BackColor = System.Drawing.Color.Transparent;
            this.btn수집시작.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn수집시작.Location = new System.Drawing.Point(293, 26);
            this.btn수집시작.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn수집시작.Name = "btn수집시작";
            this.btn수집시작.Size = new System.Drawing.Size(94, 27);
            this.btn수집시작.TabIndex = 2;
            this.btn수집시작.Text = "수집시작";
            this.btn수집시작.UseVisualStyleBackColor = false;
            this.btn수집시작.Click += new System.EventHandler(this.btn수집시작_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(133, 30);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(150, 24);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.Text = "선택하세요!";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(10, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "TERMINAL-NO";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Impact", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.DimGray;
            this.label16.Location = new System.Drawing.Point(15, 10);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(97, 23);
            this.label16.TabIndex = 2;
            this.label16.Text = "Sunil dyfas";
            // 
            // timer1
            // 
            this.timer1.Interval = 5000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick_1);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(1036, 9);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(27, 12);
            this.label22.TabIndex = 3;
            this.label22.Text = "v1.0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1075, 502);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "생산카운터 수집기";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button btn수집시작;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox txt현재카운트2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txt현재카운트3;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txt현재카운트4;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txt현재카운트5;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txt현재카운트6;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txt현재카운트1;
        private System.Windows.Forms.TextBox txt최종시간6;
        private System.Windows.Forms.TextBox txt최종시간5;
        private System.Windows.Forms.TextBox txt최종시간4;
        private System.Windows.Forms.TextBox txt최종시간3;
        private System.Windows.Forms.TextBox txt최종시간2;
        private System.Windows.Forms.TextBox txt최종시간1;
        private System.Windows.Forms.TextBox txt직전카운트6;
        private System.Windows.Forms.TextBox txt직전카운트5;
        private System.Windows.Forms.TextBox txt직전카운트4;
        private System.Windows.Forms.TextBox txt직전카운트3;
        private System.Windows.Forms.TextBox txt직전카운트2;
        private System.Windows.Forms.TextBox txt직전카운트1;
        private System.Windows.Forms.TextBox txt직전시간6;
        private System.Windows.Forms.TextBox txt직전시간5;
        private System.Windows.Forms.TextBox txt직전시간4;
        private System.Windows.Forms.TextBox txt직전시간3;
        private System.Windows.Forms.TextBox txt직전시간2;
        private System.Windows.Forms.TextBox txt직전시간1;
        private System.Windows.Forms.TextBox txt설비코드6;
        private System.Windows.Forms.TextBox txt설비코드5;
        private System.Windows.Forms.TextBox txt설비코드4;
        private System.Windows.Forms.TextBox txt설비코드3;
        private System.Windows.Forms.TextBox txt설비코드2;
        private System.Windows.Forms.TextBox txt설비코드1;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label16;
        private MySql.Data.MySqlClient.MySqlConnection mySqlConnection1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btn정보설정1;
        private System.Windows.Forms.Button btn수집중지;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label22;
    }
}

