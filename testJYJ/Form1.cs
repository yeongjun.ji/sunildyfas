﻿using Microsoft.Win32; // Registry key클래스 사용
using MySql.Data.MySqlClient; // mysql추가
using System;
using System.IO;
using System.Windows.Forms;

// reg PLC COUNT6의 데이터 DB로 전송 TEST
namespace testJYJ
{
    public partial class Form1 : Form
    {
        int NUMnewcounter = 0;
        int NUMsavecounter = 0;
        int NUMnewcounter2 = 0;
        int NUMsavecounter2 = 0;
        int NUMnewcounter3 = 0;
        int NUMsavecounter3 = 0;
        int NUMnewcounter4 = 0;
        int NUMsavecounter4 = 0;
        int NUMnewcounter5 = 0;
        int NUMsavecounter5 = 0;
        int NUMnewcounter6 = 0;
        int NUMsavecounter6 = 0;

        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            string reginifolder = @"C:\reginifile";
            if (!Directory.Exists(reginifolder))
            {
                Directory.CreateDirectory(reginifolder);
            }

            string Conn = "Server=13.209.223.244;Port=3306;Database=sunildyfas_db;Uid=root;Pwd=anfmsah12#;SSLMode=None;";
            MySqlConnection conn = new MySqlConnection(Conn);
            try
            {
                conn.Open();
                timer1.Enabled = true;
                conn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            conn.Close();
        }


        void getdata()
        {
            IniFile ini = new IniFile();
            ini.Load(@"C:\reginifile\reg1.ini");
            string regroute = ini["Registry setting"]["경로"].ToString();
            string hn_idx = ini["Registry setting"]["hn_idx"].ToString();
            string code = ini["Registry setting"]["설비코드"].ToString();
            string timeold = ini["Registry Setting"]["직전시간"].ToString();
            string countold = ini["Registry Setting"]["직전카운터"].ToString();
            string counttime = ini["Registry Setting"]["최종시각"].ToString();
            string count = ini["Registry Setting"]["현재카운터"].ToString();

            string Rcode = code.Replace("PLC_COUNT_CODE", "");
            string Rtimeold = timeold.Replace("PLC_COUNT_TIME_OLD", "");
            string Rcountold = countold.Replace("PLC_COUNT_OLD", "");
            string Rcounttime = counttime.Replace("PLC_COUNT_TIME", "");
            string Rcount = count.Replace("PLC_COUNT", "");

            IniFile ini2 = new IniFile();
            ini2.Load(@"C:\reginifile\reg2.ini");
            string regroute2 = ini2["Registry setting"]["경로"].ToString();
            string hn_idx2 = ini2["Registry setting"]["hn_idx"].ToString();
            string code2 = ini2["Registry setting"]["설비코드"].ToString();
            string timeold2 = ini2["Registry Setting"]["직전시간"].ToString();
            string countold2 = ini2["Registry Setting"]["직전카운터"].ToString();
            string counttime2 = ini2["Registry Setting"]["최종시각"].ToString();
            string count2 = ini2["Registry Setting"]["현재카운터"].ToString();

            string Rcode2 = code2.Replace("PLC_COUNT_CODE", "");
            string Rtimeold2 = timeold2.Replace("PLC_COUNT_TIME_OLD", "");
            string Rcountold2 = countold2.Replace("PLC_COUNT_OLD", "");
            string Rcounttime2 = counttime2.Replace("PLC_COUNT_TIME", "");
            string Rcount2 = count2.Replace("PLC_COUNT", "");

            IniFile ini3 = new IniFile();
            ini3.Load(@"C:\reginifile\reg3.ini");
            string regroute3 = ini3["Registry setting"]["경로"].ToString();
            string hn_idx3 = ini3["Registry setting"]["hn_idx"].ToString();
            string code3 = ini3["Registry setting"]["설비코드"].ToString();
            string timeold3 = ini3["Registry Setting"]["직전시간"].ToString();
            string countold3 = ini3["Registry Setting"]["직전카운터"].ToString();
            string counttime3 = ini3["Registry Setting"]["최종시각"].ToString();
            string count3 = ini3["Registry Setting"]["현재카운터"].ToString();

            string Rcode3 = code3.Replace("PLC_COUNT_CODE", "");
            string Rtimeold3 = timeold3.Replace("PLC_COUNT_TIME_OLD", "");
            string Rcountold3 = countold3.Replace("PLC_COUNT_OLD", "");
            string Rcounttime3 = counttime3.Replace("PLC_COUNT_TIME", "");
            string Rcount3 = count3.Replace("PLC_COUNT", "");

            IniFile ini4 = new IniFile();
            ini4.Load(@"C:\reginifile\reg4.ini");
            string regroute4 = ini4["Registry setting"]["경로"].ToString();
            string hn_idx4 = ini4["Registry setting"]["hn_idx"].ToString();
            string code4 = ini4["Registry setting"]["설비코드"].ToString();
            string timeold4 = ini4["Registry Setting"]["직전시간"].ToString();
            string countold4 = ini4["Registry Setting"]["직전카운터"].ToString();
            string counttime4 = ini4["Registry Setting"]["최종시각"].ToString();
            string count4 = ini4["Registry Setting"]["현재카운터"].ToString();

            string Rcode4 = code4.Replace("PLC_COUNT_CODE", "");
            string Rtimeold4 = timeold4.Replace("PLC_COUNT_TIME_OLD", "");
            string Rcountold4 = countold4.Replace("PLC_COUNT_OLD", "");
            string Rcounttime4 = counttime4.Replace("PLC_COUNT_TIME", "");
            string Rcount4 = count4.Replace("PLC_COUNT", "");

            IniFile ini5 = new IniFile();
            ini5.Load(@"C:\reginifile\reg5.ini");
            string regroute5 = ini5["Registry setting"]["경로"].ToString();
            string hn_idx5 = ini5["Registry setting"]["hn_idx"].ToString();
            string code5 = ini5["Registry setting"]["설비코드"].ToString();
            string timeold5 = ini5["Registry Setting"]["직전시간"].ToString();
            string countold5 = ini5["Registry Setting"]["직전카운터"].ToString();
            string counttime5 = ini5["Registry Setting"]["최종시각"].ToString();
            string count5 = ini5["Registry Setting"]["현재카운터"].ToString();

            string Rcode5 = code5.Replace("PLC_COUNT_CODE", "");
            string Rtimeold5 = timeold5.Replace("PLC_COUNT_TIME_OLD", "");
            string Rcountold5 = countold5.Replace("PLC_COUNT_OLD", "");
            string Rcounttime5 = counttime5.Replace("PLC_COUNT_TIME", "");
            string Rcount5 = count5.Replace("PLC_COUNT", "");

            IniFile ini6 = new IniFile();
            ini6.Load(@"C:\reginifile\reg6.ini");
            string regroute6 = ini6["Registry setting"]["경로"].ToString();
            string hn_idx6 = ini6["Registry setting"]["hn_idx"].ToString();
            string code6 = ini6["Registry setting"]["설비코드"].ToString();
            string timeold6 = ini6["Registry Setting"]["직전시간"].ToString();
            string countold6 = ini6["Registry Setting"]["직전카운터"].ToString();
            string counttime6 = ini6["Registry Setting"]["최종시각"].ToString();
            string count6 = ini6["Registry Setting"]["현재카운터"].ToString();

            string Rcode6 = code6.Replace("PLC_COUNT_CODE", "");
            string Rtimeold6 = timeold6.Replace("PLC_COUNT_TIME_OLD", "");
            string Rcountold6 = countold6.Replace("PLC_COUNT_OLD", "");
            string Rcounttime6 = counttime6.Replace("PLC_COUNT_TIME", "");
            string Rcount6 = count6.Replace("PLC_COUNT", "");

            if (regroute != "")
            {
                using (RegistryKey rk = Registry.CurrentUser.OpenSubKey(@regroute, true))
                {
                    try
                    {
                        //레지스트리 설비코드 데이터 GET
                        string rcode1 = (string)rk.GetValue(code);
                        //레지스트리 직전시간 데이터 GET
                        string roldtime1 = (string)rk.GetValue(timeold);
                        //레지스트리 직전 카운터 데이터 GET
                        string roldcount1 = (string)rk.GetValue(countold);
                        //레지스트리 최종시각 데이터 GET
                        string rlasttime1 = (string)rk.GetValue(counttime);
                        //레지스트리 현재 카운터 데이터 GET
                        string rlastcount1 = (string)rk.GetValue(count);
                        //설비코드 텍스트박스 표시
                        txt설비코드1.Text = rcode1;
                        //직전시각 텍스트박스 표시
                        txt직전시간1.Text = roldtime1;
                        //직전카운터 텍스트박스 표시
                        txt직전카운트1.Text = roldcount1;
                        //최종시각 텍스트박스 표시
                        txt최종시간1.Text = rlasttime1;
                        //현재카운터 텍스트박스 표시
                        txt현재카운트1.Text = rlastcount1;


                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    rk.Close();




                    string conn = "Server=13.209.223.244;Port=3306;Database=sunildyfas_db;Uid=root;Pwd=anfmsah12#;SSLMode=None;";

                    string updatequery1 = "update hmi_now set save_datetime = '" + txt직전시간1.Text + "', " +
                                            "save_counter = '" + Convert.ToInt32(txt직전카운트1.Text) +
                                         "', datetime = '" + txt최종시간1.Text +
                                         "', counter = '" + Convert.ToInt32(txt현재카운트1.Text) +
                                          "' where hn_idx = '" + Convert.ToInt32(hn_idx) + "'";


                    using (MySqlConnection con = new MySqlConnection(conn))
                    {
                        con.Open();

                        MySqlTransaction tran = con.BeginTransaction(); // 트랜잭션 시작
                        MySqlCommand cmd = new MySqlCommand();

                        cmd.Connection = con;
                        cmd.Transaction = tran; // 트랜잭션 객체지정

                        MySqlCommand cmd_update1 = new MySqlCommand(updatequery1, con);

                        try
                        {
                            cmd.CommandText = updatequery1; //쿼리지정
                            cmd_update1.ExecuteNonQuery(); //실행
                            tran.Commit(); // 트랜잭션 커밋
                            con.Close();

                        }
                        catch
                        {
                            tran.Rollback();
                        }
                        con.Close();
                    }
                }
            }
            if (regroute2 != "")
            {
                using (RegistryKey rk = Registry.CurrentUser.OpenSubKey(@regroute2, true))
                {
                    try
                    {
                        //레지스트리 설비코드 데이터 GET
                        string rcode2 = (string)rk.GetValue(code2);
                        //레지스트리 직전시간 데이터 GET
                        string roldtime2 = (string)rk.GetValue(timeold2);
                        //레지스트리 직전 카운터 데이터 GET
                        string roldcount2 = (string)rk.GetValue(countold2);
                        //레지스트리 최종시각 데이터 GET
                        string rlasttime2 = (string)rk.GetValue(counttime2);
                        //레지스트리 현재 카운터 데이터 GET
                        string rlastcount2 = (string)rk.GetValue(count2);
                        //설비코드 텍스트박스 표시
                        txt설비코드2.Text = rcode2;
                        //직전시각 텍스트박스 표시
                        txt직전시간2.Text = roldtime2;
                        //직전카운터 텍스트박스 표시
                        txt직전카운트2.Text = roldcount2;
                        //최종시각 텍스트박스 표시
                        txt최종시간2.Text = rlasttime2;
                        //현재카운터 텍스트박스 표시
                        txt현재카운트2.Text = rlastcount2;

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    rk.Close();

                    string conn = "Server=13.209.223.244;Port=3306;Database=sunildyfas_db;Uid=root;Pwd=anfmsah12#;SSLMode=None;";

                    string updatequery2 = "update hmi_now set save_datetime = '" + txt직전시간2.Text + "', " +
                                            "save_counter = '" + Convert.ToInt32(txt직전카운트2.Text) +
                                         "', datetime = '" + txt최종시간2.Text +
                                         "', counter = '" + Convert.ToInt32(txt현재카운트2.Text) +
                                          "' where hn_idx = '" + Convert.ToInt32(hn_idx2) + "'";


                    using (MySqlConnection con = new MySqlConnection(conn))
                    {
                        con.Open();

                        MySqlTransaction tran = con.BeginTransaction(); // 트랜잭션 시작
                        MySqlCommand cmd = new MySqlCommand();

                        cmd.Connection = con;
                        cmd.Transaction = tran; // 트랜잭션 객체지정

                        MySqlCommand cmd_update2 = new MySqlCommand(updatequery2, con);

                        try
                        {
                            cmd.CommandText = updatequery2; //쿼리지정
                            cmd_update2.ExecuteNonQuery(); //실행
                            tran.Commit(); // 트랜잭션 커밋
                            con.Close();

                        }
                        catch
                        {
                            tran.Rollback();
                        }
                        con.Close();
                    }
                }
            }
            if (regroute3 != "")
            {
                using (RegistryKey rk = Registry.CurrentUser.OpenSubKey(@regroute3, true))
                {
                    try
                    {
                        //레지스트리 설비코드 데이터 GET
                        string rcode3 = (string)rk.GetValue(code3);
                        //레지스트리 직전시간 데이터 GET
                        string roldtime3 = (string)rk.GetValue(timeold3);
                        //레지스트리 직전 카운터 데이터 GET
                        string roldcount3 = (string)rk.GetValue(countold3);
                        //레지스트리 최종시각 데이터 GET
                        string rlasttime3 = (string)rk.GetValue(counttime3);
                        //레지스트리 현재 카운터 데이터 GET
                        string rlastcount3 = (string)rk.GetValue(count3);
                        //설비코드 텍스트박스 표시
                        txt설비코드3.Text = rcode3;
                        //직전시각 텍스트박스 표시
                        txt직전시간3.Text = roldtime3;
                        //직전카운터 텍스트박스 표시
                        txt직전카운트3.Text = roldcount3;
                        //최종시각 텍스트박스 표시
                        txt최종시간3.Text = rlasttime3;
                        //현재카운터 텍스트박스 표시
                        txt현재카운트3.Text = rlastcount3;

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    rk.Close();

                    string conn = "Server=13.209.223.244;Port=3306;Database=sunildyfas_db;Uid=root;Pwd=anfmsah12#;SSLMode=None;";

                    string updatequery3 = "update hmi_now set save_datetime = '" + txt직전시간3.Text + "', " +
                                            "save_counter = '" + Convert.ToInt32(txt직전카운트3.Text) +
                                         "', datetime = '" + txt최종시간3.Text +
                                         "', counter = '" + Convert.ToInt32(txt현재카운트3.Text) +
                                          "' where hn_idx = '" + Convert.ToInt32(hn_idx3) + "'";


                    using (MySqlConnection con = new MySqlConnection(conn))
                    {
                        con.Open();

                        MySqlTransaction tran = con.BeginTransaction(); // 트랜잭션 시작
                        MySqlCommand cmd = new MySqlCommand();

                        cmd.Connection = con;
                        cmd.Transaction = tran; // 트랜잭션 객체지정

                        MySqlCommand cmd_update3 = new MySqlCommand(updatequery3, con);

                        try
                        {
                            cmd.CommandText = updatequery3; //쿼리지정
                            cmd_update3.ExecuteNonQuery(); //실행
                            tran.Commit(); // 트랜잭션 커밋
                            con.Close();

                        }
                        catch
                        {
                            tran.Rollback();
                        }
                        con.Close();
                    }
                }
            }
            if (regroute4 != "")
            {
                using (RegistryKey rk = Registry.CurrentUser.OpenSubKey(@regroute4, true))
                {
                    try
                    {
                        //레지스트리 설비코드 데이터 GET
                        string rcode4 = (string)rk.GetValue(code4);
                        //레지스트리 직전시간 데이터 GET
                        string roldtime4 = (string)rk.GetValue(timeold4);
                        //레지스트리 직전 카운터 데이터 GET
                        string roldcount4 = (string)rk.GetValue(countold4);
                        //레지스트리 최종시각 데이터 GET
                        string rlasttime4 = (string)rk.GetValue(counttime4);
                        //레지스트리 현재 카운터 데이터 GET
                        string rlastcount4 = (string)rk.GetValue(count4);
                        //설비코드 텍스트박스 표시
                        txt설비코드4.Text = rcode4;
                        //직전시각 텍스트박스 표시
                        txt직전시간4.Text = roldtime4;
                        //직전카운터 텍스트박스 표시
                        txt직전카운트4.Text = roldcount4;
                        //최종시각 텍스트박스 표시
                        txt최종시간4.Text = rlasttime4;
                        //현재카운터 텍스트박스 표시
                        txt현재카운트4.Text = rlastcount4;

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    rk.Close();

                    string conn = "Server=13.209.223.244;Port=3306;Database=sunildyfas_db;Uid=root;Pwd=anfmsah12#;SSLMode=None;";

                    string updatequery4 = "update hmi_now set save_datetime = '" + txt직전시간4.Text + "', " +
                                            "save_counter = '" + Convert.ToInt32(txt직전카운트4.Text) +
                                         "', datetime = '" + txt최종시간4.Text +
                                         "', counter = '" + Convert.ToInt32(txt현재카운트4.Text) +
                                          "' where hn_idx = '" + Convert.ToInt32(hn_idx4) + "'";


                    using (MySqlConnection con = new MySqlConnection(conn))
                    {
                        con.Open();

                        MySqlTransaction tran = con.BeginTransaction(); // 트랜잭션 시작
                        MySqlCommand cmd = new MySqlCommand();

                        cmd.Connection = con;
                        cmd.Transaction = tran; // 트랜잭션 객체지정

                        MySqlCommand cmd_update4 = new MySqlCommand(updatequery4, con);

                        try
                        {
                            cmd.CommandText = updatequery4; //쿼리지정
                            cmd_update4.ExecuteNonQuery(); //실행
                            tran.Commit(); // 트랜잭션 커밋
                            con.Close();

                        }
                        catch
                        {
                            tran.Rollback();
                        }
                        con.Close();
                    }
                }
            }
            if (regroute5 != "")
            {
                using (RegistryKey rk = Registry.CurrentUser.OpenSubKey(@regroute5, true))
                {
                    try
                    {
                        //레지스트리 설비코드 데이터 GET
                        string rcode5 = (string)rk.GetValue(code5);
                        //레지스트리 직전시간 데이터 GET
                        string roldtime5 = (string)rk.GetValue(timeold5);
                        //레지스트리 직전 카운터 데이터 GET
                        string roldcount5 = (string)rk.GetValue(countold5);
                        //레지스트리 최종시각 데이터 GET
                        string rlasttime5 = (string)rk.GetValue(counttime5);
                        //레지스트리 현재 카운터 데이터 GET
                        string rlastcount5 = (string)rk.GetValue(count5);
                        //설비코드 텍스트박스 표시
                        txt설비코드5.Text = rcode5;
                        //직전시각 텍스트박스 표시
                        txt직전시간5.Text = roldtime5;
                        //직전카운터 텍스트박스 표시
                        txt직전카운트5.Text = roldcount5;
                        //최종시각 텍스트박스 표시
                        txt최종시간5.Text = rlasttime5;
                        //현재카운터 텍스트박스 표시
                        txt현재카운트5.Text = rlastcount5;

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    rk.Close();

                    string conn = "Server=13.209.223.244;Port=3306;Database=sunildyfas_db;Uid=root;Pwd=anfmsah12#;SSLMode=None;";

                    string updatequery5 = "update hmi_now set save_datetime = '" + txt직전시간5.Text + "', " +
                                            "save_counter = '" + Convert.ToInt32(txt직전카운트5.Text) +
                                         "', datetime = '" + txt최종시간5.Text +
                                         "', counter = '" + Convert.ToInt32(txt현재카운트5.Text) +
                                          "' where hn_idx = '" + Convert.ToInt32(hn_idx5) + "'";


                    using (MySqlConnection con = new MySqlConnection(conn))
                    {
                        con.Open();

                        MySqlTransaction tran = con.BeginTransaction(); // 트랜잭션 시작
                        MySqlCommand cmd = new MySqlCommand();

                        cmd.Connection = con;
                        cmd.Transaction = tran; // 트랜잭션 객체지정

                        MySqlCommand cmd_update5 = new MySqlCommand(updatequery5, con);

                        try
                        {
                            cmd.CommandText = updatequery5; //쿼리지정
                            cmd_update5.ExecuteNonQuery(); //실행
                            tran.Commit(); // 트랜잭션 커밋
                            con.Close();

                        }
                        catch
                        {
                            tran.Rollback();
                        }
                        con.Close();
                    }
                }
            }
            if (regroute6 != "")
            {
                using (RegistryKey rk = Registry.CurrentUser.OpenSubKey(@regroute6, true))
                {
                    try
                    {
                        //레지스트리 설비코드 데이터 GET
                        string rcode6 = (string)rk.GetValue(code6);
                        //레지스트리 직전시간 데이터 GET
                        string roldtime6 = (string)rk.GetValue(timeold6);
                        //레지스트리 직전 카운터 데이터 GET
                        string roldcount6 = (string)rk.GetValue(countold6);
                        //레지스트리 최종시각 데이터 GET
                        string rlasttime6 = (string)rk.GetValue(counttime6);
                        //레지스트리 현재 카운터 데이터 GET
                        string rlastcount6 = (string)rk.GetValue(count6);
                        //설비코드 텍스트박스 표시
                        txt설비코드6.Text = rcode6;
                        //직전시각 텍스트박스 표시
                        txt직전시간6.Text = roldtime6;
                        //직전카운터 텍스트박스 표시
                        txt직전카운트6.Text = roldcount6;
                        //최종시각 텍스트박스 표시
                        txt최종시간6.Text = rlasttime6;
                        //현재카운터 텍스트박스 표시
                        txt현재카운트6.Text = rlastcount6;

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    rk.Close();

                    string conn = "Server=13.209.223.244;Port=3306;Database=sunildyfas_db;Uid=root;Pwd=anfmsah12#;SSLMode=None;";

                    string updatequery6 = "update hmi_now set save_datetime = '" + txt직전시간6.Text + "', " +
                                            "save_counter = '" + Convert.ToInt32(txt직전카운트6.Text) +
                                         "', datetime = '" + txt최종시간6.Text +
                                         "', counter = '" + Convert.ToInt32(txt현재카운트6.Text) +
                                          "' where hn_idx = '" + Convert.ToInt32(hn_idx6) + "'";


                    using (MySqlConnection con = new MySqlConnection(conn))
                    {
                        con.Open();

                        MySqlTransaction tran = con.BeginTransaction(); // 트랜잭션 시작
                        MySqlCommand cmd = new MySqlCommand();

                        cmd.Connection = con;
                        cmd.Transaction = tran; // 트랜잭션 객체지정

                        MySqlCommand cmd_update6 = new MySqlCommand(updatequery6, con);

                        try
                        {
                            cmd.CommandText = updatequery6; //쿼리지정
                            cmd_update6.ExecuteNonQuery(); //실행
                            tran.Commit(); // 트랜잭션 커밋
                            con.Close();

                        }
                        catch
                        {
                            tran.Rollback();
                        }
                        con.Close();
                    }
                }
            }



        }


        private void timer1_Tick_1(object sender, EventArgs e)
        {
            IniFile ini = new IniFile();
            ini.Load(@"C:\reginifile\reg1.ini");
            string regroute = ini["Registry setting"]["경로"].ToString();
            string hn_idx = ini["Registry setting"]["hn_idx"].ToString();
            string code = ini["Registry setting"]["설비코드"].ToString();
            string timeold = ini["Registry Setting"]["직전시간"].ToString();
            string countold = ini["Registry Setting"]["직전카운터"].ToString();
            string counttime = ini["Registry Setting"]["최종시각"].ToString();
            string count = ini["Registry Setting"]["현재카운터"].ToString();

            string Rcode = code.Replace("PLC_COUNT_CODE", "");
            string Rtimeold = timeold.Replace("PLC_COUNT_TIME_OLD", "");
            string Rcountold = countold.Replace("PLC_COUNT_OLD", "");
            string Rcounttime = counttime.Replace("PLC_COUNT_TIME", "");
            string Rcount = count.Replace("PLC_COUNT", "");

            IniFile ini2 = new IniFile();
            ini2.Load(@"C:\reginifile\reg2.ini");
            string regroute2 = ini2["Registry setting"]["경로"].ToString();
            string hn_idx2 = ini2["Registry setting"]["hn_idx"].ToString();
            string code2 = ini2["Registry setting"]["설비코드"].ToString();
            string timeold2 = ini2["Registry Setting"]["직전시간"].ToString();
            string countold2 = ini2["Registry Setting"]["직전카운터"].ToString();
            string counttime2 = ini2["Registry Setting"]["최종시각"].ToString();
            string count2 = ini2["Registry Setting"]["현재카운터"].ToString();

            string Rcode2 = code2.Replace("PLC_COUNT_CODE", "");
            string Rtimeold2 = timeold2.Replace("PLC_COUNT_TIME_OLD", "");
            string Rcountold2 = countold2.Replace("PLC_COUNT_OLD", "");
            string Rcounttime2 = counttime2.Replace("PLC_COUNT_TIME", "");
            string Rcount2 = count2.Replace("PLC_COUNT", "");

            IniFile ini3 = new IniFile();
            ini3.Load(@"C:\reginifile\reg3.ini");
            string regroute3 = ini3["Registry setting"]["경로"].ToString();
            string hn_idx3 = ini3["Registry setting"]["hn_idx"].ToString();
            string code3 = ini3["Registry setting"]["설비코드"].ToString();
            string timeold3 = ini3["Registry Setting"]["직전시간"].ToString();
            string countold3 = ini3["Registry Setting"]["직전카운터"].ToString();
            string counttime3 = ini3["Registry Setting"]["최종시각"].ToString();
            string count3 = ini3["Registry Setting"]["현재카운터"].ToString();

            string Rcode3 = code3.Replace("PLC_COUNT_CODE", "");
            string Rtimeold3 = timeold3.Replace("PLC_COUNT_TIME_OLD", "");
            string Rcountold3 = countold3.Replace("PLC_COUNT_OLD", "");
            string Rcounttime3 = counttime3.Replace("PLC_COUNT_TIME", "");
            string Rcount3 = count3.Replace("PLC_COUNT", "");

            IniFile ini4 = new IniFile();
            ini4.Load(@"C:\reginifile\reg4.ini");
            string regroute4 = ini4["Registry setting"]["경로"].ToString();
            string hn_idx4 = ini4["Registry setting"]["hn_idx"].ToString();
            string code4 = ini4["Registry setting"]["설비코드"].ToString();
            string timeold4 = ini4["Registry Setting"]["직전시간"].ToString();
            string countold4 = ini4["Registry Setting"]["직전카운터"].ToString();
            string counttime4 = ini4["Registry Setting"]["최종시각"].ToString();
            string count4 = ini4["Registry Setting"]["현재카운터"].ToString();

            string Rcode4 = code4.Replace("PLC_COUNT_CODE", "");
            string Rtimeold4 = timeold4.Replace("PLC_COUNT_TIME_OLD", "");
            string Rcountold4 = countold4.Replace("PLC_COUNT_OLD", "");
            string Rcounttime4 = counttime4.Replace("PLC_COUNT_TIME", "");
            string Rcount4 = count4.Replace("PLC_COUNT", "");

            IniFile ini5 = new IniFile();
            ini5.Load(@"C:\reginifile\reg5.ini");
            string regroute5 = ini5["Registry setting"]["경로"].ToString();
            string hn_idx5 = ini5["Registry setting"]["hn_idx"].ToString();
            string code5 = ini5["Registry setting"]["설비코드"].ToString();
            string timeold5 = ini5["Registry Setting"]["직전시간"].ToString();
            string countold5 = ini5["Registry Setting"]["직전카운터"].ToString();
            string counttime5 = ini5["Registry Setting"]["최종시각"].ToString();
            string count5 = ini5["Registry Setting"]["현재카운터"].ToString();

            string Rcode5 = code5.Replace("PLC_COUNT_CODE", "");
            string Rtimeold5 = timeold5.Replace("PLC_COUNT_TIME_OLD", "");
            string Rcountold5 = countold5.Replace("PLC_COUNT_OLD", "");
            string Rcounttime5 = counttime5.Replace("PLC_COUNT_TIME", "");
            string Rcount5 = count5.Replace("PLC_COUNT", "");

            IniFile ini6 = new IniFile();
            ini6.Load(@"C:\reginifile\reg6.ini");
            string regroute6 = ini6["Registry setting"]["경로"].ToString();
            string hn_idx6 = ini6["Registry setting"]["hn_idx"].ToString();
            string code6 = ini6["Registry setting"]["설비코드"].ToString();
            string timeold6 = ini6["Registry Setting"]["직전시간"].ToString();
            string countold6 = ini6["Registry Setting"]["직전카운터"].ToString();
            string counttime6 = ini6["Registry Setting"]["최종시각"].ToString();
            string count6 = ini6["Registry Setting"]["현재카운터"].ToString();

            string Rcode6 = code6.Replace("PLC_COUNT_CODE", "");
            string Rtimeold6 = timeold6.Replace("PLC_COUNT_TIME_OLD", "");
            string Rcountold6 = countold6.Replace("PLC_COUNT_OLD", "");
            string Rcounttime6 = counttime6.Replace("PLC_COUNT_TIME", "");
            string Rcount6 = count6.Replace("PLC_COUNT", "");
            if (regroute != "")
            {

                using (RegistryKey rk = Registry.CurrentUser.OpenSubKey(@regroute, true))
                {
                    try
                    {
                        //레지스트리 설비코드 데이터 GET
                        string rcode1 = (string)rk.GetValue(code);
                        //레지스트리 직전시간 데이터 GET
                        string roldtime1 = (string)rk.GetValue(timeold);
                        //레지스트리 직전 카운터 데이터 GET
                        string roldcount1 = (string)rk.GetValue(countold);
                        //레지스트리 최종시각 데이터 GET
                        string rlasttime1 = (string)rk.GetValue(counttime);
                        //레지스트리 현재 카운터 데이터 GET
                        string rlastcount1 = (string)rk.GetValue(count);
                        //설비코드 텍스트박스 표시
                        txt설비코드1.Text = rcode1;
                        //직전시각 텍스트박스 표시
                        txt직전시간1.Text = roldtime1;
                        //직전카운터 텍스트박스 표시
                        txt직전카운트1.Text = roldcount1;
                        //최종시각 텍스트박스 표시
                        txt최종시간1.Text = rlasttime1;
                        //현재카운터 텍스트박스 표시
                        txt현재카운트1.Text = rlastcount1;

                        //레지스트리 현재 카운터 데이터 GET
                        int NUMrlastcount = Convert.ToInt32(rlastcount1);

                        if (NUMrlastcount == 0)
                        {
                            NUMsavecounter = NUMrlastcount;
                        }
                        else if (NUMrlastcount > NUMsavecounter)
                        {
                            NUMnewcounter = NUMrlastcount - NUMsavecounter;
                            NUMsavecounter = NUMrlastcount;

                            string conn = "Server=13.209.223.244;Port=3306;Database=sunildyfas_db;Uid=root;Pwd=anfmsah12#;SSLMode=None;";

                            string updatequery1 = "update hmi_now set save_datetime = '" + txt직전시간1.Text + "', " +
                                                    "save_counter = '" + Convert.ToInt32(txt직전카운트1.Text) +
                                                 "', datetime = '" + txt최종시간1.Text +
                                                 "', counter = '" + Convert.ToInt32(txt현재카운트1.Text) +
                                                  "' where hn_idx = '" + Convert.ToInt32(hn_idx) + "'";

                            string savecounterquery1 = "update hmi_now set today_counter = today_counter + " + NUMnewcounter + " where hn_idx = '" + Convert.ToInt32(hn_idx) + "'";
                            //string savecounterquery1 = "update hmi_now set today_counter = today_counter + 5 where hn_idx = '" + Convert.ToInt32(hn_idx) + "'";
                            using (MySqlConnection con = new MySqlConnection(conn))
                            {
                                con.Open();

                                MySqlTransaction tran = con.BeginTransaction(); // 트랜잭션 시작
                                MySqlCommand cmd = new MySqlCommand();

                                cmd.Connection = con;
                                cmd.Transaction = tran; // 트랜잭션 객체지정

                                MySqlCommand cmd_update1 = new MySqlCommand(updatequery1, con);
                                MySqlCommand cmd_update1_1 = new MySqlCommand(savecounterquery1, con);
                                try
                                {
                                    cmd.CommandText = updatequery1; //쿼리지정
                                    cmd_update1.ExecuteNonQuery(); //실행
                                    cmd.CommandText = savecounterquery1; //쿼리지정
                                    cmd_update1_1.ExecuteNonQuery(); //실행
                                    tran.Commit(); // 트랜잭션 커밋
                                    con.Close();

                                }
                                catch
                                {
                                    tran.Rollback();
                                }
                                con.Close();
                            }
                        }
                        else
                        {
                            getdata();
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                    rk.Close();
                }
            }
            if (regroute2 != "")
            {

                using (RegistryKey rk = Registry.CurrentUser.OpenSubKey(@regroute2, true))
                {
                    try
                    {
                        //레지스트리 설비코드 데이터 GET
                        string rcode2 = (string)rk.GetValue(code2);
                        //레지스트리 직전시간 데이터 GET
                        string roldtime2 = (string)rk.GetValue(timeold2);
                        //레지스트리 직전 카운터 데이터 GET
                        string roldcount2 = (string)rk.GetValue(countold2);
                        //레지스트리 최종시각 데이터 GET
                        string rlasttime2 = (string)rk.GetValue(counttime2);
                        //레지스트리 현재 카운터 데이터 GET
                        string rlastcount2 = (string)rk.GetValue(count2);
                        //설비코드 텍스트박스 표시
                        txt설비코드2.Text = rcode2;
                        //직전시각 텍스트박스 표시
                        txt직전시간2.Text = roldtime2;
                        //직전카운터 텍스트박스 표시
                        txt직전카운트2.Text = roldcount2;
                        //최종시각 텍스트박스 표시
                        txt최종시간2.Text = rlasttime2;
                        //현재카운터 텍스트박스 표시
                        txt현재카운트2.Text = rlastcount2;

                        //레지스트리 현재 카운터 데이터 GET
                        int NUMrlastcount2 = Convert.ToInt32(rlastcount2);

                        if (NUMrlastcount2 == 0)
                        {
                            NUMsavecounter2 = NUMrlastcount2;
                        }
                        else if (NUMrlastcount2 > NUMsavecounter2)
                        {
                            NUMnewcounter2 = NUMrlastcount2 - NUMsavecounter2;
                            NUMsavecounter2 = NUMrlastcount2;

                            string conn = "Server=13.209.223.244;Port=3306;Database=sunildyfas_db;Uid=root;Pwd=anfmsah12#;SSLMode=None;";

                            string updatequery2 = "update hmi_now set save_datetime = '" + txt직전시간2.Text + "', " +
                                                    "save_counter = '" + Convert.ToInt32(txt직전카운트2.Text) +
                                                 "', datetime = '" + txt최종시간2.Text +
                                                 "', counter = '" + Convert.ToInt32(txt현재카운트2.Text) +
                                                  "' where hn_idx = '" + Convert.ToInt32(hn_idx2) + "'";

                            string savecounterquery2 = "update hmi_now set today_counter = today_counter + " + NUMnewcounter2 + " where hn_idx = '" + Convert.ToInt32(hn_idx2) + "'";
                            //string savecounterquery1 = "update hmi_now set today_counter = today_counter + 5 where hn_idx = '" + Convert.ToInt32(hn_idx) + "'";
                            using (MySqlConnection con = new MySqlConnection(conn))
                            {
                                con.Open();

                                MySqlTransaction tran = con.BeginTransaction(); // 트랜잭션 시작
                                MySqlCommand cmd = new MySqlCommand();

                                cmd.Connection = con;
                                cmd.Transaction = tran; // 트랜잭션 객체지정

                                MySqlCommand cmd_update2 = new MySqlCommand(updatequery2, con);
                                MySqlCommand cmd_update2_2 = new MySqlCommand(savecounterquery2, con);
                                try
                                {
                                    cmd.CommandText = updatequery2; //쿼리지정
                                    cmd_update2.ExecuteNonQuery(); //실행
                                    cmd.CommandText = savecounterquery2; //쿼리지정
                                    cmd_update2_2.ExecuteNonQuery(); //실행
                                    tran.Commit(); // 트랜잭션 커밋
                                    con.Close();

                                }
                                catch
                                {
                                    tran.Rollback();
                                }
                                con.Close();
                            }
                        }
                        else
                        {
                            getdata();
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                    rk.Close();
                }
            }
            if (regroute3 != "")
            {

                using (RegistryKey rk = Registry.CurrentUser.OpenSubKey(@regroute3, true))
                {
                    try
                    {
                        //레지스트리 설비코드 데이터 GET
                        string rcode3 = (string)rk.GetValue(code3);
                        //레지스트리 직전시간 데이터 GET
                        string roldtime3 = (string)rk.GetValue(timeold3);
                        //레지스트리 직전 카운터 데이터 GET
                        string roldcount3 = (string)rk.GetValue(countold3);
                        //레지스트리 최종시각 데이터 GET
                        string rlasttime3 = (string)rk.GetValue(counttime3);
                        //레지스트리 현재 카운터 데이터 GET
                        string rlastcount3 = (string)rk.GetValue(count3);
                        //설비코드 텍스트박스 표시
                        txt설비코드3.Text = rcode3;
                        //직전시각 텍스트박스 표시
                        txt직전시간3.Text = roldtime3;
                        //직전카운터 텍스트박스 표시
                        txt직전카운트3.Text = roldcount3;
                        //최종시각 텍스트박스 표시
                        txt최종시간3.Text = rlasttime3;
                        //현재카운터 텍스트박스 표시
                        txt현재카운트3.Text = rlastcount3;

                        //레지스트리 현재 카운터 데이터 GET
                        int NUMrlastcount3 = Convert.ToInt32(rlastcount3);

                        if (NUMrlastcount3 == 0)
                        {
                            NUMsavecounter3 = NUMrlastcount3;
                        }
                        else if (NUMrlastcount3 > NUMsavecounter)
                        {
                            NUMnewcounter3 = NUMrlastcount3 - NUMsavecounter3;
                            NUMsavecounter3 = NUMrlastcount3;

                            string conn = "Server=13.209.223.244;Port=3306;Database=sunildyfas_db;Uid=root;Pwd=anfmsah12#;SSLMode=None;";

                            string updatequery3 = "update hmi_now set save_datetime = '" + txt직전시간3.Text + "', " +
                                                    "save_counter = '" + Convert.ToInt32(txt직전카운트3.Text) +
                                                 "', datetime = '" + txt최종시간3.Text +
                                                 "', counter = '" + Convert.ToInt32(txt현재카운트3.Text) +
                                                  "' where hn_idx = '" + Convert.ToInt32(hn_idx3) + "'";

                            string savecounterquery3 = "update hmi_now set today_counter = today_counter + " + NUMnewcounter3 + " where hn_idx = '" + Convert.ToInt32(hn_idx3) + "'";
                            //string savecounterquery1 = "update hmi_now set today_counter = today_counter + 5 where hn_idx = '" + Convert.ToInt32(hn_idx) + "'";
                            using (MySqlConnection con = new MySqlConnection(conn))
                            {
                                con.Open();

                                MySqlTransaction tran = con.BeginTransaction(); // 트랜잭션 시작
                                MySqlCommand cmd = new MySqlCommand();

                                cmd.Connection = con;
                                cmd.Transaction = tran; // 트랜잭션 객체지정

                                MySqlCommand cmd_update3 = new MySqlCommand(updatequery3, con);
                                MySqlCommand cmd_update3_3 = new MySqlCommand(savecounterquery3, con);
                                try
                                {
                                    cmd.CommandText = updatequery3; //쿼리지정
                                    cmd_update3.ExecuteNonQuery(); //실행
                                    cmd.CommandText = savecounterquery3; //쿼리지정
                                    cmd_update3_3.ExecuteNonQuery(); //실행
                                    tran.Commit(); // 트랜잭션 커밋
                                    con.Close();

                                }
                                catch
                                {
                                    tran.Rollback();
                                }
                                con.Close();
                            }
                        }
                        else
                        {
                            getdata();
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                    rk.Close();
                }
            }
            if (regroute4 != "")
            {

                using (RegistryKey rk = Registry.CurrentUser.OpenSubKey(@regroute4, true))
                {
                    try
                    {
                        //레지스트리 설비코드 데이터 GET
                        string rcode4 = (string)rk.GetValue(code4);
                        //레지스트리 직전시간 데이터 GET
                        string roldtime4 = (string)rk.GetValue(timeold4);
                        //레지스트리 직전 카운터 데이터 GET
                        string roldcount4 = (string)rk.GetValue(countold4);
                        //레지스트리 최종시각 데이터 GET
                        string rlasttime4 = (string)rk.GetValue(counttime4);
                        //레지스트리 현재 카운터 데이터 GET
                        string rlastcount4 = (string)rk.GetValue(count4);
                        //설비코드 텍스트박스 표시
                        txt설비코드4.Text = rcode4;
                        //직전시각 텍스트박스 표시
                        txt직전시간4.Text = roldtime4;
                        //직전카운터 텍스트박스 표시
                        txt직전카운트4.Text = roldcount4;
                        //최종시각 텍스트박스 표시
                        txt최종시간4.Text = rlasttime4;
                        //현재카운터 텍스트박스 표시
                        txt현재카운트4.Text = rlastcount4;

                        //레지스트리 현재 카운터 데이터 GET
                        int NUMrlastcount4 = Convert.ToInt32(rlastcount4);

                        if (NUMrlastcount4 == 0)
                        {
                            NUMsavecounter4 = NUMrlastcount4;
                        }
                        else if (NUMrlastcount4 > NUMsavecounter4)
                        {
                            NUMnewcounter4 = NUMrlastcount4 - NUMsavecounter4;
                            NUMsavecounter4 = NUMrlastcount4;

                            string conn = "Server=13.209.223.244;Port=3306;Database=sunildyfas_db;Uid=root;Pwd=anfmsah12#;SSLMode=None;";

                            string updatequery4 = "update hmi_now set save_datetime = '" + txt직전시간4.Text + "', " +
                                                    "save_counter = '" + Convert.ToInt32(txt직전카운트4.Text) +
                                                 "', datetime = '" + txt최종시간4.Text +
                                                 "', counter = '" + Convert.ToInt32(txt현재카운트4.Text) +
                                                  "' where hn_idx = '" + Convert.ToInt32(hn_idx4) + "'";

                            string savecounterquery4 = "update hmi_now set today_counter = today_counter + " + NUMnewcounter4 + " where hn_idx = '" + Convert.ToInt32(hn_idx4) + "'";
                            //string savecounterquery1 = "update hmi_now set today_counter = today_counter + 5 where hn_idx = '" + Convert.ToInt32(hn_idx) + "'";
                            using (MySqlConnection con = new MySqlConnection(conn))
                            {
                                con.Open();

                                MySqlTransaction tran = con.BeginTransaction(); // 트랜잭션 시작
                                MySqlCommand cmd = new MySqlCommand();

                                cmd.Connection = con;
                                cmd.Transaction = tran; // 트랜잭션 객체지정

                                MySqlCommand cmd_update4 = new MySqlCommand(updatequery4, con);
                                MySqlCommand cmd_update4_4 = new MySqlCommand(savecounterquery4, con);
                                try
                                {
                                    cmd.CommandText = updatequery4; //쿼리지정
                                    cmd_update4.ExecuteNonQuery(); //실행
                                    cmd.CommandText = savecounterquery4; //쿼리지정
                                    cmd_update4_4.ExecuteNonQuery(); //실행
                                    tran.Commit(); // 트랜잭션 커밋
                                    con.Close();

                                }
                                catch
                                {
                                    tran.Rollback();
                                }
                                con.Close();
                            }
                        }
                        else
                        {
                            getdata();
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                    rk.Close();
                }
            }
            if (regroute5 != "")
            {

                using (RegistryKey rk = Registry.CurrentUser.OpenSubKey(@regroute5, true))
                {
                    try
                    {
                        //레지스트리 설비코드 데이터 GET
                        string rcode5 = (string)rk.GetValue(code5);
                        //레지스트리 직전시간 데이터 GET
                        string roldtime5 = (string)rk.GetValue(timeold5);
                        //레지스트리 직전 카운터 데이터 GET
                        string roldcount5 = (string)rk.GetValue(countold5);
                        //레지스트리 최종시각 데이터 GET
                        string rlasttime5 = (string)rk.GetValue(counttime5);
                        //레지스트리 현재 카운터 데이터 GET
                        string rlastcount5 = (string)rk.GetValue(count5);
                        //설비코드 텍스트박스 표시
                        txt설비코드5.Text = rcode5;
                        //직전시각 텍스트박스 표시
                        txt직전시간5.Text = roldtime5;
                        //직전카운터 텍스트박스 표시
                        txt직전카운트5.Text = roldcount5;
                        //최종시각 텍스트박스 표시
                        txt최종시간5.Text = rlasttime5;
                        //현재카운터 텍스트박스 표시
                        txt현재카운트5.Text = rlastcount5;

                        //레지스트리 현재 카운터 데이터 GET
                        int NUMrlastcount5 = Convert.ToInt32(rlastcount5);

                        if (NUMrlastcount5 == 0)
                        {
                            NUMsavecounter5 = NUMrlastcount5;
                        }
                        else if (NUMrlastcount5 > NUMsavecounter5)
                        {
                            NUMnewcounter = NUMrlastcount5 - NUMsavecounter;
                            NUMsavecounter = NUMrlastcount5;

                            string conn = "Server=13.209.223.244;Port=3306;Database=sunildyfas_db;Uid=root;Pwd=anfmsah12#;SSLMode=None;";

                            string updatequery5 = "update hmi_now set save_datetime = '" + txt직전시간5.Text + "', " +
                                                    "save_counter = '" + Convert.ToInt32(txt직전카운트5.Text) +
                                                 "', datetime = '" + txt최종시간5.Text +
                                                 "', counter = '" + Convert.ToInt32(txt현재카운트5.Text) +
                                                  "' where hn_idx = '" + Convert.ToInt32(hn_idx5) + "'";

                            string savecounterquery5 = "update hmi_now set today_counter = today_counter + " + NUMnewcounter5 + " where hn_idx = '" + Convert.ToInt32(hn_idx5) + "'";
                            //string savecounterquery1 = "update hmi_now set today_counter = today_counter + 5 where hn_idx = '" + Convert.ToInt32(hn_idx) + "'";
                            using (MySqlConnection con = new MySqlConnection(conn))
                            {
                                con.Open();

                                MySqlTransaction tran = con.BeginTransaction(); // 트랜잭션 시작
                                MySqlCommand cmd = new MySqlCommand();

                                cmd.Connection = con;
                                cmd.Transaction = tran; // 트랜잭션 객체지정

                                MySqlCommand cmd_update5 = new MySqlCommand(updatequery5, con);
                                MySqlCommand cmd_update5_5 = new MySqlCommand(savecounterquery5, con);
                                try
                                {
                                    cmd.CommandText = updatequery5; //쿼리지정
                                    cmd_update5.ExecuteNonQuery(); //실행
                                    cmd.CommandText = savecounterquery5; //쿼리지정
                                    cmd_update5_5.ExecuteNonQuery(); //실행
                                    tran.Commit(); // 트랜잭션 커밋
                                    con.Close();

                                }
                                catch
                                {
                                    tran.Rollback();
                                }
                                con.Close();
                            }
                        }
                        else
                        {
                            getdata();
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                    rk.Close();
                }
            }
            if (regroute6 != "")
            {

                using (RegistryKey rk = Registry.CurrentUser.OpenSubKey(@regroute6, true))
                {
                    try
                    {
                        //레지스트리 설비코드 데이터 GET
                        string rcode6 = (string)rk.GetValue(code6);
                        //레지스트리 직전시간 데이터 GET
                        string roldtime6 = (string)rk.GetValue(timeold6);
                        //레지스트리 직전 카운터 데이터 GET
                        string roldcount6 = (string)rk.GetValue(countold6);
                        //레지스트리 최종시각 데이터 GET
                        string rlasttime6 = (string)rk.GetValue(counttime6);
                        //레지스트리 현재 카운터 데이터 GET
                        string rlastcount6 = (string)rk.GetValue(count6);
                        //설비코드 텍스트박스 표시
                        txt설비코드6.Text = rcode6;
                        //직전시각 텍스트박스 표시
                        txt직전시간6.Text = roldtime6;
                        //직전카운터 텍스트박스 표시
                        txt직전카운트6.Text = roldcount6;
                        //최종시각 텍스트박스 표시
                        txt최종시간6.Text = rlasttime6;
                        //현재카운터 텍스트박스 표시
                        txt현재카운트6.Text = rlastcount6;

                        //레지스트리 현재 카운터 데이터 GET
                        int NUMrlastcount6 = Convert.ToInt32(rlastcount6);

                        if (NUMrlastcount6 == 0)
                        {
                            NUMsavecounter6 = NUMrlastcount6;
                        }
                        else if (NUMrlastcount6 > NUMsavecounter6)
                        {
                            NUMnewcounter6 = NUMrlastcount6 - NUMsavecounter6;
                            NUMsavecounter6 = NUMrlastcount6;

                            string conn = "Server=13.209.223.244;Port=3306;Database=sunildyfas_db;Uid=root;Pwd=anfmsah12#;SSLMode=None;";

                            string updatequery6 = "update hmi_now set save_datetime = '" + txt직전시간6.Text + "', " +
                                                    "save_counter = '" + Convert.ToInt32(txt직전카운트6.Text) +
                                                 "', datetime = '" + txt최종시간6.Text +
                                                 "', counter = '" + Convert.ToInt32(txt현재카운트6.Text) +
                                                  "' where hn_idx = '" + Convert.ToInt32(hn_idx6) + "'";

                            string savecounterquery6 = "update hmi_now set today_counter = today_counter + " + NUMnewcounter6 + " where hn_idx = '" + Convert.ToInt32(hn_idx6) + "'";
                            //string savecounterquery1 = "update hmi_now set today_counter = today_counter + 5 where hn_idx = '" + Convert.ToInt32(hn_idx) + "'";
                            using (MySqlConnection con = new MySqlConnection(conn))
                            {
                                con.Open();

                                MySqlTransaction tran = con.BeginTransaction(); // 트랜잭션 시작
                                MySqlCommand cmd = new MySqlCommand();

                                cmd.Connection = con;
                                cmd.Transaction = tran; // 트랜잭션 객체지정

                                MySqlCommand cmd_update6 = new MySqlCommand(updatequery6, con);
                                MySqlCommand cmd_update6_6 = new MySqlCommand(savecounterquery6, con);
                                try
                                {
                                    cmd.CommandText = updatequery6; //쿼리지정
                                    cmd_update6.ExecuteNonQuery(); //실행
                                    cmd.CommandText = savecounterquery6; //쿼리지정
                                    cmd_update6_6.ExecuteNonQuery(); //실행
                                    tran.Commit(); // 트랜잭션 커밋
                                    con.Close();

                                }
                                catch
                                {
                                    tran.Rollback();
                                }
                                con.Close();
                            }
                        }
                        else
                        {
                            getdata();
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                    rk.Close();
                }
            }
        }


        private void btn정보설정_Click(object sender, EventArgs e)
        {
            reginfo1 newreginfo1 = new reginfo1();
            newreginfo1.ShowDialog();
        }

        private void btn수집시작_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            try
            {
                MessageBox.Show("DATA수집 시작");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btn수집중지_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            try
            {
                MessageBox.Show("DATA수집 중지");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            reginfo2 newreginfo2 = new reginfo2();
            newreginfo2.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            reginfo3 newreginfo3 = new reginfo3();
            newreginfo3.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            reginfo4 newreginfo4 = new reginfo4();
            newreginfo4.ShowDialog();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            reginfo5 newreginfo5 = new reginfo5();
            newreginfo5.ShowDialog();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            reginfo6 newreginfo6 = new reginfo6();
            newreginfo6.ShowDialog();
        }
    }
}
