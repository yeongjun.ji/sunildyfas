﻿
namespace testJYJ
{
    partial class reginfo1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txt설비코드 = new System.Windows.Forms.TextBox();
            this.txt직전시간 = new System.Windows.Forms.TextBox();
            this.txt직전카운터 = new System.Windows.Forms.TextBox();
            this.txt최종시각 = new System.Windows.Forms.TextBox();
            this.txt현재카운터 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txt경로 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.texthn = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(214, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(283, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "MCH-01 레지스트리 정보 설정";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(384, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(242, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "PLC_COUNT_CODE (설비코드) :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(345, 162);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(281, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "PLC_COUNT__TIME_OLD (직전시간) :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(380, 198);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(246, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "PLC_COUNT_OLD (직전카운터) :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(392, 232);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(234, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "PLC_COUNT_TIME (최종시각) :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(419, 266);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(207, 16);
            this.label6.TabIndex = 5;
            this.label6.Text = "PLC_COUNT (현재카운터) :";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.RoyalBlue;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(645, 306);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(78, 27);
            this.button1.TabIndex = 6;
            this.button1.Text = "입력";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txt설비코드
            // 
            this.txt설비코드.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt설비코드.Location = new System.Drawing.Point(651, 125);
            this.txt설비코드.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt설비코드.Name = "txt설비코드";
            this.txt설비코드.Size = new System.Drawing.Size(69, 21);
            this.txt설비코드.TabIndex = 7;
            // 
            // txt직전시간
            // 
            this.txt직전시간.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt직전시간.Location = new System.Drawing.Point(651, 158);
            this.txt직전시간.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt직전시간.Name = "txt직전시간";
            this.txt직전시간.Size = new System.Drawing.Size(69, 21);
            this.txt직전시간.TabIndex = 8;
            // 
            // txt직전카운터
            // 
            this.txt직전카운터.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt직전카운터.Location = new System.Drawing.Point(651, 194);
            this.txt직전카운터.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt직전카운터.Name = "txt직전카운터";
            this.txt직전카운터.Size = new System.Drawing.Size(69, 21);
            this.txt직전카운터.TabIndex = 9;
            // 
            // txt최종시각
            // 
            this.txt최종시각.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt최종시각.Location = new System.Drawing.Point(651, 229);
            this.txt최종시각.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt최종시각.Name = "txt최종시각";
            this.txt최종시각.Size = new System.Drawing.Size(69, 21);
            this.txt최종시각.TabIndex = 10;
            // 
            // txt현재카운터
            // 
            this.txt현재카운터.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt현재카운터.Location = new System.Drawing.Point(651, 263);
            this.txt현재카운터.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt현재카운터.Name = "txt현재카운터";
            this.txt현재카운터.Size = new System.Drawing.Size(69, 21);
            this.txt현재카운터.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(10, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(191, 16);
            this.label8.TabIndex = 13;
            this.label8.Text = "HKEY_CURRENT_USER\\";
            // 
            // txt경로
            // 
            this.txt경로.Location = new System.Drawing.Point(10, 73);
            this.txt경로.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt경로.Name = "txt경로";
            this.txt경로.Size = new System.Drawing.Size(727, 21);
            this.txt경로.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(682, 98);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 15;
            this.label7.Text = "경로설정";
            // 
            // texthn
            // 
            this.texthn.Location = new System.Drawing.Point(83, 125);
            this.texthn.Name = "texthn";
            this.texthn.Size = new System.Drawing.Size(69, 21);
            this.texthn.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(14, 127);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 16);
            this.label9.TabIndex = 18;
            this.label9.Text = "hn_idx :";
            // 
            // reginfo1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(748, 349);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.texthn);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txt경로);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txt현재카운터);
            this.Controls.Add(this.txt최종시각);
            this.Controls.Add(this.txt직전카운터);
            this.Controls.Add(this.txt직전시간);
            this.Controls.Add(this.txt설비코드);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "reginfo1";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.reginfo1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txt설비코드;
        private System.Windows.Forms.TextBox txt직전시간;
        private System.Windows.Forms.TextBox txt직전카운터;
        private System.Windows.Forms.TextBox txt최종시각;
        private System.Windows.Forms.TextBox txt현재카운터;
        private System.Windows.Forms.Label label8;
        internal System.Windows.Forms.TextBox txt경로;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox texthn;
        private System.Windows.Forms.Label label9;
    }
}