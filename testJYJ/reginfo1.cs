﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace testJYJ
{
    public partial class reginfo1 : Form
    {
        public reginfo1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // ini 쓰기
            IniFile ini = new IniFile();
            ini["Registry setting"]["경로"] = txt경로.Text;
            ini["Registry setting"]["hn_idx"] = texthn.Text;
            ini["Registry setting"]["설비코드"] = "PLC_COUNT_CODE" + txt설비코드.Text;
            ini["Registry Setting"]["직전시간"] = "PLC_COUNT_TIME_OLD" + txt직전시간.Text;
            ini["Registry Setting"]["직전카운터"] = "PLC_COUNT_OLD" + txt직전카운터.Text;
            ini["Registry Setting"]["최종시각"] = "PLC_COUNT_TIME" + txt최종시각.Text;
            ini["Registry Setting"]["현재카운터"] = "PLC_COUNT" + txt현재카운터.Text;
            ini.Save(@"C:\reginifile\reg1.ini");
            try
            {
                MessageBox.Show("Registry information setting 완료");
                this.Close();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void reginfo1_Load(object sender, EventArgs e)
        {
            IniFile ini = new IniFile();
            ini.Load(@"C:\reginifile\reg1.ini");
            string regroute = ini["Registry setting"]["경로"].ToString();
            string hn_idx = ini["Registry setting"]["hn_idx"].ToString();
            string code = ini["Registry setting"]["설비코드"].ToString();
            string timeold = ini["Registry Setting"]["직전시간"].ToString();
            string countold = ini["Registry Setting"]["직전카운터"].ToString();
            string counttime = ini["Registry Setting"]["최종시각"].ToString();
            string count = ini["Registry Setting"]["현재카운터"].ToString();

            string Rcode = code.Replace("PLC_COUNT_CODE", "");
            string Rtimeold = timeold.Replace("PLC_COUNT_TIME_OLD", "");
            string Rcountold = countold.Replace("PLC_COUNT_OLD", "");
            string Rcounttime = counttime.Replace("PLC_COUNT_TIME", "");
            string Rcount = count.Replace("PLC_COUNT", "");

            txt경로.Text = regroute;
            texthn.Text = hn_idx;
            txt설비코드.Text = Rcode;
            txt직전시간.Text = Rtimeold;
            txt직전카운터.Text = Rcountold;
            txt최종시각.Text = Rcounttime;
            txt현재카운터.Text = Rcount;
        }
    }
}
