﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace testJYJ
{
    public partial class reginfo2 : Form
    {
        public reginfo2()
        {
            InitializeComponent();
        }

        private void btn입력2_Click(object sender, EventArgs e)
        {
            // ini 쓰기
            IniFile ini2 = new IniFile();
            ini2["Registry setting"]["경로"] = txt경로2.Text;
            ini2["Registry setting"]["hn_idx"] = texthn2.Text;
            ini2["Registry setting"]["설비코드"] = "PLC_COUNT_CODE" + txt설비코드2.Text;
            ini2["Registry Setting"]["직전시간"] = "PLC_COUNT_TIME_OLD" + txt직전시간2.Text;
            ini2["Registry Setting"]["직전카운터"] = "PLC_COUNT_OLD" + txt직전카운터2.Text;
            ini2["Registry Setting"]["최종시각"] = "PLC_COUNT_TIME" + txt최종시각2.Text;
            ini2["Registry Setting"]["현재카운터"] = "PLC_COUNT" + txt현재카운터2.Text;
            ini2.Save(@"C:\reginifile\reg2.ini");
            try
            {
                MessageBox.Show("Registry information setting 완료");
                this.Close();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void reginfo2_Load(object sender, EventArgs e)
        {
            IniFile ini2 = new IniFile();
            ini2.Load(@"C:\reginifile\reg2.ini");
            string regroute2 = ini2["Registry setting"]["경로"].ToString();
            string hn_idx2 = ini2["Registry setting"]["hn_idx"].ToString();
            string code2 = ini2["Registry setting"]["설비코드"].ToString();
            string timeold2 = ini2["Registry Setting"]["직전시간"].ToString();
            string countold2 = ini2["Registry Setting"]["직전카운터"].ToString();
            string counttime2 = ini2["Registry Setting"]["최종시각"].ToString();
            string count2 = ini2["Registry Setting"]["현재카운터"].ToString();

            string Rcode2 = code2.Replace("PLC_COUNT_CODE", "");
            string Rtimeold2 = timeold2.Replace("PLC_COUNT_TIME_OLD", "");
            string Rcountold2 = countold2.Replace("PLC_COUNT_OLD", "");
            string Rcounttime2 = counttime2.Replace("PLC_COUNT_TIME", "");
            string Rcount2 = count2.Replace("PLC_COUNT", "");

            txt경로2.Text = regroute2;
            texthn2.Text = hn_idx2;
            txt설비코드2.Text = Rcode2;
            txt직전시간2.Text = Rtimeold2;
            txt직전카운터2.Text = Rcountold2;
            txt최종시각2.Text = Rcounttime2;
            txt현재카운터2.Text = Rcount2;
        }
    }
}
