﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace testJYJ
{
    public partial class reginfo3 : Form
    {
        public reginfo3()
        {
            InitializeComponent();
        }

        private void reginfo3_Load(object sender, EventArgs e)
        {
            IniFile ini3 = new IniFile();
            ini3.Load(@"C:\reginifile\reg3.ini");
            string regroute3 = ini3["Registry setting"]["경로"].ToString();
            string hn_idx3 = ini3["Registry setting"]["hn_idx"].ToString();
            string code3 = ini3["Registry setting"]["설비코드"].ToString();
            string timeold3 = ini3["Registry Setting"]["직전시간"].ToString();
            string countold3 = ini3["Registry Setting"]["직전카운터"].ToString();
            string counttime3 = ini3["Registry Setting"]["최종시각"].ToString();
            string count3 = ini3["Registry Setting"]["현재카운터"].ToString();

            string Rcode3 = code3.Replace("PLC_COUNT_CODE", "");
            string Rtimeold3 = timeold3.Replace("PLC_COUNT_TIME_OLD", "");
            string Rcountold3 = countold3.Replace("PLC_COUNT_OLD", "");
            string Rcounttime3 = counttime3.Replace("PLC_COUNT_TIME", "");
            string Rcount3 = count3.Replace("PLC_COUNT", "");

            txt경로3.Text = regroute3;
            texthn3.Text = hn_idx3;
            txt설비코드3.Text = Rcode3;
            txt직전시간3.Text = Rtimeold3;
            txt직전카운터3.Text = Rcountold3;
            txt최종시각3.Text = Rcounttime3;
            txt현재카운터3.Text = Rcount3;
        }

        private void btn입력2_Click(object sender, EventArgs e)
        {

            // ini 쓰기
            IniFile ini3 = new IniFile();
            ini3["Registry setting"]["경로"] = txt경로3.Text;
            ini3["Registry setting"]["hn_idx"] = texthn3.Text;
            ini3["Registry setting"]["설비코드"] = "PLC_COUNT_CODE" + txt설비코드3.Text;
            ini3["Registry Setting"]["직전시간"] = "PLC_COUNT_TIME_OLD" + txt직전시간3.Text;
            ini3["Registry Setting"]["직전카운터"] = "PLC_COUNT_OLD" + txt직전카운터3.Text;
            ini3["Registry Setting"]["최종시각"] = "PLC_COUNT_TIME" + txt최종시각3.Text;
            ini3["Registry Setting"]["현재카운터"] = "PLC_COUNT" + txt현재카운터3.Text;
            ini3.Save(@"C:\reginifile\reg3.ini");
            try
            {
                MessageBox.Show("Registry information setting 완료");
                this.Close();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
