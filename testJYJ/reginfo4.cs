﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace testJYJ
{
    public partial class reginfo4 : Form
    {
        public reginfo4()
        {
            InitializeComponent();
        }

        private void reginfo4_Load(object sender, EventArgs e)
        {
            IniFile ini4 = new IniFile();
            ini4.Load(@"C:\reginifile\reg4.ini");
            string regroute4 = ini4["Registry setting"]["경로"].ToString();
            string hn_idx4 = ini4["Registry setting"]["hn_idx"].ToString();
            string code4 = ini4["Registry setting"]["설비코드"].ToString();
            string timeold4 = ini4["Registry Setting"]["직전시간"].ToString();
            string countold4 = ini4["Registry Setting"]["직전카운터"].ToString();
            string counttime4 = ini4["Registry Setting"]["최종시각"].ToString();
            string count4 = ini4["Registry Setting"]["현재카운터"].ToString();

            string Rcode4 = code4.Replace("PLC_COUNT_CODE", "");
            string Rtimeold4 = timeold4.Replace("PLC_COUNT_TIME_OLD", "");
            string Rcountold4 = countold4.Replace("PLC_COUNT_OLD", "");
            string Rcounttime4 = counttime4.Replace("PLC_COUNT_TIME", "");
            string Rcount4 = count4.Replace("PLC_COUNT", "");

            txt경로4.Text = regroute4;
            texthn4.Text = hn_idx4;
            txt설비코드4.Text = Rcode4;
            txt직전시간4.Text = Rtimeold4;
            txt직전카운터4.Text = Rcountold4;
            txt최종시각4.Text = Rcounttime4;
            txt현재카운터4.Text = Rcount4;
        }

        private void btn입력2_Click(object sender, EventArgs e)
        {
            // ini 쓰기
            IniFile ini4 = new IniFile();
            ini4["Registry setting"]["경로"] = txt경로4.Text;
            ini4["Registry setting"]["hn_idx"] = texthn4.Text;
            ini4["Registry setting"]["설비코드"] = "PLC_COUNT_CODE" + txt설비코드4.Text;
            ini4["Registry Setting"]["직전시간"] = "PLC_COUNT_TIME_OLD" + txt직전시간4.Text;
            ini4["Registry Setting"]["직전카운터"] = "PLC_COUNT_OLD" + txt직전카운터4.Text;
            ini4["Registry Setting"]["최종시각"] = "PLC_COUNT_TIME" + txt최종시각4.Text;
            ini4["Registry Setting"]["현재카운터"] = "PLC_COUNT" + txt현재카운터4.Text;
            ini4.Save(@"C:\reginifile\reg4.ini");
            try
            {
                MessageBox.Show("Registry information setting 완료");
                this.Close();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
