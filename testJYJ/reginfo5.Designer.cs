﻿
namespace testJYJ
{
    partial class reginfo5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label9 = new System.Windows.Forms.Label();
            this.texthn5 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txt경로5 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txt현재카운터5 = new System.Windows.Forms.TextBox();
            this.txt최종시각5 = new System.Windows.Forms.TextBox();
            this.txt직전카운터5 = new System.Windows.Forms.TextBox();
            this.txt직전시간5 = new System.Windows.Forms.TextBox();
            this.txt설비코드5 = new System.Windows.Forms.TextBox();
            this.btn입력2 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(42, 151);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 16);
            this.label9.TabIndex = 52;
            this.label9.Text = "hn_idx :";
            // 
            // texthn5
            // 
            this.texthn5.Location = new System.Drawing.Point(111, 149);
            this.texthn5.Name = "texthn5";
            this.texthn5.Size = new System.Drawing.Size(69, 21);
            this.texthn5.TabIndex = 51;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(710, 122);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 50;
            this.label7.Text = "경로설정";
            // 
            // txt경로5
            // 
            this.txt경로5.Location = new System.Drawing.Point(38, 97);
            this.txt경로5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt경로5.Name = "txt경로5";
            this.txt경로5.Size = new System.Drawing.Size(727, 21);
            this.txt경로5.TabIndex = 49;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(38, 74);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(191, 16);
            this.label8.TabIndex = 48;
            this.label8.Text = "HKEY_CURRENT_USER\\";
            // 
            // txt현재카운터5
            // 
            this.txt현재카운터5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt현재카운터5.Location = new System.Drawing.Point(679, 287);
            this.txt현재카운터5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt현재카운터5.Name = "txt현재카운터5";
            this.txt현재카운터5.Size = new System.Drawing.Size(69, 21);
            this.txt현재카운터5.TabIndex = 47;
            // 
            // txt최종시각5
            // 
            this.txt최종시각5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt최종시각5.Location = new System.Drawing.Point(679, 253);
            this.txt최종시각5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt최종시각5.Name = "txt최종시각5";
            this.txt최종시각5.Size = new System.Drawing.Size(69, 21);
            this.txt최종시각5.TabIndex = 46;
            // 
            // txt직전카운터5
            // 
            this.txt직전카운터5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt직전카운터5.Location = new System.Drawing.Point(679, 218);
            this.txt직전카운터5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt직전카운터5.Name = "txt직전카운터5";
            this.txt직전카운터5.Size = new System.Drawing.Size(69, 21);
            this.txt직전카운터5.TabIndex = 45;
            // 
            // txt직전시간5
            // 
            this.txt직전시간5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt직전시간5.Location = new System.Drawing.Point(679, 182);
            this.txt직전시간5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt직전시간5.Name = "txt직전시간5";
            this.txt직전시간5.Size = new System.Drawing.Size(69, 21);
            this.txt직전시간5.TabIndex = 44;
            // 
            // txt설비코드5
            // 
            this.txt설비코드5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt설비코드5.Location = new System.Drawing.Point(679, 149);
            this.txt설비코드5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt설비코드5.Name = "txt설비코드5";
            this.txt설비코드5.Size = new System.Drawing.Size(69, 21);
            this.txt설비코드5.TabIndex = 43;
            // 
            // btn입력2
            // 
            this.btn입력2.BackColor = System.Drawing.Color.RoyalBlue;
            this.btn입력2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn입력2.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn입력2.ForeColor = System.Drawing.Color.White;
            this.btn입력2.Location = new System.Drawing.Point(673, 330);
            this.btn입력2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn입력2.Name = "btn입력2";
            this.btn입력2.Size = new System.Drawing.Size(78, 27);
            this.btn입력2.TabIndex = 42;
            this.btn입력2.Text = "입력";
            this.btn입력2.UseVisualStyleBackColor = false;
            this.btn입력2.Click += new System.EventHandler(this.btn입력2_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(447, 290);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(207, 16);
            this.label6.TabIndex = 41;
            this.label6.Text = "PLC_COUNT (현재카운터) :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(420, 256);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(234, 16);
            this.label5.TabIndex = 40;
            this.label5.Text = "PLC_COUNT_TIME (최종시각) :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(408, 222);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(246, 16);
            this.label4.TabIndex = 39;
            this.label4.Text = "PLC_COUNT_OLD (직전카운터) :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(373, 186);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(281, 16);
            this.label3.TabIndex = 38;
            this.label3.Text = "PLC_COUNT__TIME_OLD (직전시간) :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(412, 153);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(242, 16);
            this.label2.TabIndex = 37;
            this.label2.Text = "PLC_COUNT_CODE (설비코드) :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(242, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(283, 19);
            this.label1.TabIndex = 36;
            this.label1.Text = "MCH-01 레지스트리 정보 설정";
            // 
            // reginfo5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 383);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.texthn5);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txt경로5);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txt현재카운터5);
            this.Controls.Add(this.txt최종시각5);
            this.Controls.Add(this.txt직전카운터5);
            this.Controls.Add(this.txt직전시간5);
            this.Controls.Add(this.txt설비코드5);
            this.Controls.Add(this.btn입력2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "reginfo5";
            this.Text = "Form5";
            this.Load += new System.EventHandler(this.reginfo5_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox texthn5;
        private System.Windows.Forms.Label label7;
        internal System.Windows.Forms.TextBox txt경로5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt현재카운터5;
        private System.Windows.Forms.TextBox txt최종시각5;
        private System.Windows.Forms.TextBox txt직전카운터5;
        private System.Windows.Forms.TextBox txt직전시간5;
        private System.Windows.Forms.TextBox txt설비코드5;
        private System.Windows.Forms.Button btn입력2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}