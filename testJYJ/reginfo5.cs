﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace testJYJ
{
    public partial class reginfo5 : Form
    {
        public reginfo5()
        {
            InitializeComponent();
        }

        private void reginfo5_Load(object sender, EventArgs e)
        {
            IniFile ini5 = new IniFile();
            ini5.Load(@"C:\reginifile\reg5.ini");
            string regroute5 = ini5["Registry setting"]["경로"].ToString();
            string hn_idx5 = ini5["Registry setting"]["hn_idx"].ToString();
            string code5 = ini5["Registry setting"]["설비코드"].ToString();
            string timeold5 = ini5["Registry Setting"]["직전시간"].ToString();
            string countold5 = ini5["Registry Setting"]["직전카운터"].ToString();
            string counttime5 = ini5["Registry Setting"]["최종시각"].ToString();
            string count5 = ini5["Registry Setting"]["현재카운터"].ToString();

            string Rcode5 = code5.Replace("PLC_COUNT_CODE", "");
            string Rtimeold5 = timeold5.Replace("PLC_COUNT_TIME_OLD", "");
            string Rcountold5 = countold5.Replace("PLC_COUNT_OLD", "");
            string Rcounttime5 = counttime5.Replace("PLC_COUNT_TIME", "");
            string Rcount5 = count5.Replace("PLC_COUNT", "");

            txt경로5.Text = regroute5;
            texthn5.Text = hn_idx5;
            txt설비코드5.Text = Rcode5;
            txt직전시간5.Text = Rtimeold5;
            txt직전카운터5.Text = Rcountold5;
            txt최종시각5.Text = Rcounttime5;
            txt현재카운터5.Text = Rcount5;
        }

        private void btn입력2_Click(object sender, EventArgs e)
        {
            // ini 쓰기
            IniFile ini5 = new IniFile();
            ini5["Registry setting"]["경로"] = txt경로5.Text;
            ini5["Registry setting"]["hn_idx"] = texthn5.Text;
            ini5["Registry setting"]["설비코드"] = "PLC_COUNT_CODE" + txt설비코드5.Text;
            ini5["Registry Setting"]["직전시간"] = "PLC_COUNT_TIME_OLD" + txt직전시간5.Text;
            ini5["Registry Setting"]["직전카운터"] = "PLC_COUNT_OLD" + txt직전카운터5.Text;
            ini5["Registry Setting"]["최종시각"] = "PLC_COUNT_TIME" + txt최종시각5.Text;
            ini5["Registry Setting"]["현재카운터"] = "PLC_COUNT" + txt현재카운터5.Text;
            ini5.Save(@"C:\reginifile\reg5.ini");
            try
            {
                MessageBox.Show("Registry information setting 완료");
                this.Close();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
