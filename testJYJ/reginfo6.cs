﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace testJYJ
{
    public partial class reginfo6 : Form
    {
        public reginfo6()
        {
            InitializeComponent();
        }

        private void reginfo6_Load(object sender, EventArgs e)
        {
            IniFile ini6 = new IniFile();
            ini6.Load(@"C:\reginifile\reg6.ini");
            string regroute6 = ini6["Registry setting"]["경로"].ToString();
            string hn_idx6 = ini6["Registry setting"]["hn_idx"].ToString();
            string code6 = ini6["Registry setting"]["설비코드"].ToString();
            string timeold6 = ini6["Registry Setting"]["직전시간"].ToString();
            string countold6 = ini6["Registry Setting"]["직전카운터"].ToString();
            string counttime6 = ini6["Registry Setting"]["최종시각"].ToString();
            string count6 = ini6["Registry Setting"]["현재카운터"].ToString();

            string Rcode6 = code6.Replace("PLC_COUNT_CODE", "");
            string Rtimeold6 = timeold6.Replace("PLC_COUNT_TIME_OLD", "");
            string Rcountold6 = countold6.Replace("PLC_COUNT_OLD", "");
            string Rcounttime6 = counttime6.Replace("PLC_COUNT_TIME", "");
            string Rcount6 = count6.Replace("PLC_COUNT", "");

            txt경로6.Text = regroute6;
            texthn6.Text = hn_idx6;
            txt설비코드6.Text = Rcode6;
            txt직전시간6.Text = Rtimeold6;
            txt직전카운터6.Text = Rcountold6;
            txt최종시각6.Text = Rcounttime6;
            txt현재카운터6.Text = Rcount6;
        }

        private void btn입력6_Click(object sender, EventArgs e)
        {
            // ini 쓰기
            IniFile ini6 = new IniFile();
            ini6["Registry setting"]["경로"] = txt경로6.Text;
            ini6["Registry setting"]["hn_idx"] = texthn6.Text;
            ini6["Registry setting"]["설비코드"] = "PLC_COUNT_CODE" + txt설비코드6.Text;
            ini6["Registry Setting"]["직전시간"] = "PLC_COUNT_TIME_OLD" + txt직전시간6.Text;
            ini6["Registry Setting"]["직전카운터"] = "PLC_COUNT_OLD" + txt직전카운터6.Text;
            ini6["Registry Setting"]["최종시각"] = "PLC_COUNT_TIME" + txt최종시각6.Text;
            ini6["Registry Setting"]["현재카운터"] = "PLC_COUNT" + txt현재카운터6.Text;
            ini6.Save(@"C:\reginifile\reg6.ini");
            try
            {
                MessageBox.Show("Registry information setting 완료");
                this.Close();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
